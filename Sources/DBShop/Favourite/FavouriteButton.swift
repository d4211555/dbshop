//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 17.06.2021.
//

import Foundation
import UIKit
import Lottie

public class DBFavouriteBarButtonItem: UIBarButtonItem {
    
    public func setFavourite(_ bool: Bool) {
        
        self.image = bool ? DBImage.favouriteFilled() : DBImage.favouriteEmpty()
    }
    
    public init(target: AnyObject, action: Selector) {
        super.init()
        self.style = .done
        self.target = target
        self.action = action
        self.image =  DBImage.favouriteEmpty()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

public protocol DBFavouriteButtonDelegate: AnyObject {
    func setFavourite(bool: Bool)
}

public class DBFavouriteButton: UIView {
    
    var animationView: AnimationView!
    public var inFavourite = false
    public weak var delegate: DBFavouriteButtonDelegate?
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.addAnimatedView()
    }
    
    public init() {
        super.init(frame: CGRect.zero)
        self.addAnimatedView()
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        
        self.addAnimatedView()
    }
    
    public func addAnimatedView() {
        let configURL = Bundle.module.url(forResource: "favourite", withExtension: "json")
        animationView = AnimationView(url: configURL!) { e in }
        animationView.frame = self.bounds
            self.addSubview(animationView)
        animationView.addAction { [unowned self] in
            self.setInFavourite(!self.inFavourite)
        }
    }
    
    public func setInFavourite(_ b: Bool, animated: Bool = true) {
        if (animationView.animation == nil) {
            let seconds = 0.2
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                self.setInFav(b, animated: animated)
            }
        }else{
            self.setInFav(b, animated: animated)
        }
        
        
    }
    
    private func setInFav(_ b: Bool, animated: Bool = true) {
        if (b == self.inFavourite) {
            return
        }
        self.inFavourite = b
            let progress : CGFloat = b ? 1.0 : 0.0
            if (animated) {
                self.animationView.play(fromProgress: nil, toProgress: progress, loopMode: nil) { bb in
                    self.delegate?.setFavourite(bool: b)
                }
            }else{
                self.animationView.currentProgress = progress
            }
    }
    
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //fatalError("init(coder:) has not been implemented")
    }
    
}
