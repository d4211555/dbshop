//
//  BucketPresenter.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 06.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import RxSwift

public protocol DBFavouriteListLoadingView: DBCartLoadingView {
    func allProductsLoaded(products: [ProductT])
}

open class DBFavouriteLoadingPresenter<T: DBFavouriteListLoadingView, CoreDataCart: DBCoreDataCartProtocol, CoreDataFavourite: DBCoreDataFavouriteProtocol, M: DBProduct, CartItem: DBCartItemModel>: DBProductCartLoadingPresenter<T, CoreDataCart, CoreDataFavourite, M, CartItem> {
    
    override open func start() {
        super.start()
        DBAppLocalNotification.addLocalSubscriber(key: getClassName()+"_updateFavouriteList", localEventType: [.updateFavouriteList]) { (localEvent) in
            if (localEvent.type == DBChangesNotificationType.updateFavouriteList) {
                self.getProducts()
            }
        }
    }
    
    open func getProductsDataSource(ids: [Int]) -> Single<[M]>{
        fatalError("need to override")
    }
    
    public func getProducts() {
        guard let favouriteModels = coreDataFavouriteManager?.getAllFavouriteItems() else {
            self.view?.showEmptyItem()
            return
        }
        
        if (favouriteModels.isEmpty) {
            self.view?.showEmptyItem()
            return
        }
        
        view?.showLoading()
        let ids = favouriteModels.map { (cart) -> Int in
            return (cart as DBFavouriteItemModel).productId
        }
        getProductsDataSource(ids: ids)
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { [self] (products) in
                var deleteFavouriteArray = [DBFavouriteItemModel]()
                var favouriteArray = [DBFavouriteItemModel]()
                favouriteModels.forEach { (favourite) in
                    let p = products.first(where: { (product) -> Bool in
                        let s = product.getProductIdValue()
                        return s == favourite.productId
                    })
                    if (p == nil) {
                        deleteFavouriteArray.append(favourite)
                    }else{
                        favourite.product = p
                        favouriteArray.append(favourite)
                    }
                }
                deleteFavouriteArray.forEach { (model) in
                    let _ = coreDataFavouriteManager?.deleteFavouriteItem(productId: model.productId)
                    DBRuntimeStorageSingleton.sharedInstance.removeFavouriteItem(productId: model.productId)
                }
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateFavouritesCount)
                self.products = favouriteModels.map{$0.product as! M}
                self.view?.allProductsLoaded(products: self.products as! [T.ProductT])
                self.view?.showContent()
            }, onFailure: onAppRetryError)
            .disposed(by: disposeBag)
    }
    
    override public func addToFavourite(product: M) {
        let s = product.getProductIdValue()
        if coreDataFavouriteManager?.getFavouriteItem(productId: s) == nil {
            if coreDataFavouriteManager?.addFavouriteItem(productId: s) == true {
                DBRuntimeStorageSingleton.sharedInstance.addFavouriteItem(productId: s)
                DBAppLocalNotification.sendLocalNotification(notificationType: .productAddedToFavourite, objectId: product.getProductIdValue())
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateFavouritesCount)
            }
        }
    }
    
    override public func removeFromFavourite(product: M) {
        let s = product.getProductIdValue()
        if coreDataFavouriteManager?.deleteFavouriteItem(productId: s) == true {
            DBRuntimeStorageSingleton.sharedInstance.removeFavouriteItem(productId: s)
            DBAppLocalNotification.sendLocalNotification(notificationType: .productRemovedFromFavourite, objectId: product.getProductIdValue())
            DBAppLocalNotification.sendLocalNotification(notificationType: .updateFavouritesCount)
        }
    }
    open override func stop() {
        super.stop()
        DBAppLocalNotification.removeLocalSubscriber(key: getClassName()+"_updateFavouriteList")
    }
}

//class ProductCartPresenter<T: DBCartLoadingView>:
//
//    required init(view: T?) {
//        super.init(view: view, coreDataCartManager: CoreDataEngine.sharedInstance, coreDataFavouriteManager: CoreDataEngine.sharedInstance)
//    }
//}
