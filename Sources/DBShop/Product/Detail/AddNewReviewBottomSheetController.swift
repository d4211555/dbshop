//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 23.08.2021.
//

import Foundation
import UIKit

public class AddNewReviewBottomSheetController: DBBottomSheetViewController {
    var textView: UITextView!
    var ratingView: DBFloatRatingView!
    var bottomConstraint: NSLayoutConstraint!
    var contentView: UIView!
    public var onAddReview : ((_ ratingValue: Int, _ comment: String?)->Void)?
    

    public init() {
        contentView = UIView()
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        ratingView = DBFloatRatingView()
        ratingView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(ratingView)
        ratingView.layoutAttachTop(margin: 8)
        ratingView.layoutAttachLeading(margin: 16)
        ratingView.layoutAttachTrailing(margin: 16)
        ratingView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = DBStrings.commentTitle + ":"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        contentView.addSubview(label)
        label.topAnchor.constraint(equalTo: ratingView.bottomAnchor, constant: 16).isActive = true
        label.layoutAttachLeading(margin: 16)
        label.layoutAttachTrailing(margin: 16)
        
        textView = UITextView()
        textView.setBorderColor()
        textView.font = UIFont.systemFont(ofSize: 17)
        textView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(textView)
        textView.layoutAttachLeading(margin: 16)
        textView.layoutAttachTrailing(margin: 16)
        textView.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 8).isActive = true
        textView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        
       
        
        let button = DBButton()
        button.primary()
        button.setTitle(DBStrings.addReview, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(button)
        button.topAnchor.constraint(equalTo: textView.bottomAnchor, constant: 16).isActive = true
        button.layoutAttachLeading(margin: 16)
        button.layoutAttachTrailing(margin: 16)
        bottomConstraint = button.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16)
        bottomConstraint.isActive = true
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        super.init(contentView: contentView, title: DBStrings.addNewReview, top: 0, right: 0, left: 0, bottom: 0)
        button.addAction { [unowned self] in
            if (ratingView.rating == 0.0) {
                self.view.makeToast(DBStrings.pleaseRateProduct, point: self.view.center, title: nil, image: nil, completion: nil)
                return
            }
            self.onAddReview?(Int(ratingView.rating), textView.text == "" ? nil : textView.text)
        }
        addKeyboardNotification()
    }
    
    public func addKeyboardNotification(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(AddNewReviewBottomSheetController.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(AddNewReviewBottomSheetController.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
//        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTap))
//        tap.cancelsTouchesInView = true
//        self.view.addGestureRecognizer(tap)
    }
    
    @objc public func keyboardWillShow(notification: NSNotification) {
        guard let size = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else {
            return
        }
        self.bottomConstraint.constant = -size.height
        UIView.animate(withDuration: 5.0) {
            self.view.layoutIfNeeded()
        }
    }
    
    
    @objc public func keyboardWillHide(notification: NSNotification) {
        self.bottomConstraint.constant = -16
        UIView.animate(withDuration: 5.0) {
            self.view.layoutIfNeeded()
        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
extension UIView
{
    func copyView<T: UIView>() -> T {
        return NSKeyedUnarchiver.unarchiveObject(with: NSKeyedArchiver.archivedData(withRootObject: self)) as! T
    }
}
