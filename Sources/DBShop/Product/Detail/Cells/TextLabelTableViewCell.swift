//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 03.06.2021.
//

import Foundation
import UIKit
import DBTableCollectionView

public class DBTextLabelItem: DBModel {
    var text: String!
    var textColor: UIColor!
    var isBold: Bool!
    var isSeparator: Bool!
    var fontSize: CGFloat!
    var accessoryType: UITableViewCell.AccessoryType!
    var icon: UIImage?
    public var notificationCount: Int?
    public var tag: Int!
    var selectionStyle: UITableViewCell.SelectionStyle!
    var textLabelInteractionEnabled: Bool!
    
    public init(text: String, textColor: UIColor = .black, isBold: Bool = false, fontSize: CGFloat = 17, accessoryType: UITableViewCell.AccessoryType = .none, isSeparator: Bool = true, icon: UIImage? = nil, notificationCount: Int? = nil, tag: Int = 0, selectionStyle: UITableViewCell.SelectionStyle = .none, textLabelInteractionEnabled: Bool = false) {
        self.text = text
        self.isBold = isBold
        self.fontSize = fontSize
        self.accessoryType = accessoryType
        self.isSeparator = isSeparator
        self.icon = icon
        self.notificationCount = notificationCount
        self.tag = tag
        self.selectionStyle = selectionStyle
        self.textColor = textColor
        self.textLabelInteractionEnabled = textLabelInteractionEnabled
    }
    
}

public protocol DBTextLabelTableViewCellDelegate: DBProtocol {
    func accessoryButtonTapped(item: DBTextLabelItem)
    func cellDidSelect(item: DBTextLabelItem)
}

extension DBTextLabelTableViewCellDelegate {
    func accessoryButtonTapped(item: DBTextLabelItem){
        
    }
}

public class DBTextLabelTableViewCell: DBTableViewCell<DBTextLabelItem, DBTextLabelTableViewCellDelegate> {
     
    var stackView: UIStackView!
    var label: DBTextViewLikeLabel!
    var notificationLabel: NotificationLabel!
    var iconImageView: UIImageView!
    
    
    class NotificationLabel: UILabel {
        var b = false
        var notificationLabelWidth: NSLayoutConstraint!
        init(){
            super.init(frame: CGRect.zero)
            self.textColor = UIColor.white
            //self.adjustsFontSizeToFitWidth = true
            self.backgroundColor = UIColor.red
            self.layer.cornerRadius = self.frame.height / 2
            self.layer.masksToBounds = true
            self.clipsToBounds = true
            self.textAlignment = .center
            self.font = UIFont.systemFont(ofSize: 14)
            //notificationLabelWidth = self.widthAnchor.constraint(equalToConstant: 20)
            //notificationLabelWidth.isActive = true
            setContentHuggingPriority(.required, for: .horizontal)
            setContentCompressionResistancePriority(.required, for: .horizontal)
            
            self.heightAnchor.constraint(equalToConstant: 20).isActive = true
        }
        
        override func draw(_ rect: CGRect) {
            super.draw(rect)
            //notificationLabelWidth.constant = rect.width + 20
        }
        
        override func drawText(in rect: CGRect) {
            super.drawText(in: rect)
            if (!b) {
                b = true
                self.widthAnchor.constraint(equalToConstant: (rect.width + 10) < 20 ? 20 : (rect.width + 10)).isActive = true
            }
            
        }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
        }

        override func layoutSubviews() {
            super.layoutSubviews()
            //notificationLabelWidth.constant = self.frame.width + 5
            //print("notificationLabelWidth.constant \(notificationLabelWidth.constant)")
            self.layer.cornerRadius = self.frame.height / 2
        }
        override func updateConstraints() {
            super.updateConstraints()
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        stackView.axis = .horizontal
        contentView.addSubview(stackView)
        stackView.layoutAttachTop(to: nil, margin: 12)
        stackView.layoutAttachBottom(to: nil, margin: 12)
        stackView.layoutAttachTrailing(to: nil, margin: 16)
        stackView.layoutAttachLeading(to: nil, margin: 16)
        
        label = DBTextViewLikeLabel()

        iconImageView = UIImageView()
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.widthAnchor.constraint(equalToConstant: 20).isActive = true
        iconImageView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        iconImageView.tintColor = DBColors.secondaryColor
        notificationLabel = NotificationLabel()
        notificationLabel.translatesAutoresizingMaskIntoConstraints = false
        

        
        //contentView.addSubview(iconImageView)
        //iconImageView.layoutAttachTop(margin: 12)
        //iconImageView.layoutAttachLeading(margin: 16)
        
//        iconImageViewTrailing = label.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 0)
//        iconImageViewTrailing.isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func configureCell(item: DBTextLabelItem) {
        self.selectionStyle = item.selectionStyle
        stackView.removeAllSubview()
        stackView.addArrangedSubview(label)
        if (item.isSeparator) {
            self.separatorInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0)
        }else{
            self.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
        }
        self.accessoryType = item.accessoryType
        
        
        self.label.textColor = item.textColor
        self.label.isUserInteractionEnabled = item.textLabelInteractionEnabled
        self.label.text = item.text
        if (item.isBold) {
            label.font = UIFont.boldSystemFont(ofSize: item.fontSize)
        }else{
            label.font = UIFont.systemFont(ofSize: item.fontSize)
        }
        if (item.icon != nil) {
            stackView.insertArrangedSubview(toView(subView: iconImageView), at: 0)
            iconImageView.image = item.icon!
        }else{
            if (iconImageView.superview != nil) {
                stackView.removeArrangedSubview(iconImageView.superview!)
            }
        }
        if (item.notificationCount != nil && item.notificationCount != 0) {
            let v = toView(subView: notificationLabel)
            stackView.addArrangedSubview(v)
            notificationLabel.text = "\(String(describing: item.notificationCount!))"
        }else{
            if (notificationLabel.superview != nil) {
                stackView.removeArrangedSubview(notificationLabel.superview!)
            }
        }
    }
    
    func toView(subView: UIView) -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        subView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subView)
        subView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
        subView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
        subView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: 0).isActive = true
        return view
    }
    
    public override func cellDidSelect(item: DBTextLabelItem) {
        getDelegate()?.cellDidSelect(item: item)
    }
    
    public override func accessoryButtonTapped(item: DBTextLabelItem) {
        getDelegate()?.accessoryButtonTapped(item: item)
        print("label frame \(label.frame)")
    }
    
    public class func getConfiguration(delegate: DBTextLabelTableViewCellDelegate?) -> DBTableViewCellConfig{
        return super.getConfiguration(delegate: delegate, nibName: nil, registerType: .registerClass)
    }
}
