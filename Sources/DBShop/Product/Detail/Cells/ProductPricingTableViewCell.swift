//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 03.06.2021.
//

import Foundation
import UIKit
import DBTableCollectionView

//
public class DBProductPriceItem: DBModel {
    var price: NSNumber!
    var previousPrice: NSNumber?
    public init(price: NSNumber, previousPrice: NSNumber?) {
        self.price = price
        self.previousPrice = previousPrice
    }
}

public class DBProductPricingTableViewCell: DBTableViewCell<DBProductPriceItem, DBProtocol> {
    
    class TestLabel: UILabel {
        override func drawText(in rect: CGRect) {
            let insets = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
            super.drawText(in: rect.inset(by: insets))
        }
    }
    
    var priceLabel: TestLabel!
    var previousPriceLabel: UILabel!
    
    public class func getConfiguration(delegate: DBProtocol?) -> DBTableViewCellConfig{
        return DBTableViewCellConfig(type: DBProductPricingTableViewCell.self, cellIdentifier: "DBProductPricingTableViewCell", delegate: delegate, modelType: DBProductPriceItem.self, registerType: .registerClass, nibName: nil)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
        priceLabel = TestLabel()
        priceLabel.translatesAutoresizingMaskIntoConstraints = false
        priceLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.contentView.addSubview(priceLabel)
        priceLabel.layoutAttachTop(margin: 8)
        priceLabel.layoutAttachBottom(margin: 8)
        priceLabel.layoutAttachLeading(margin: 16)
        priceLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        priceLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        priceLabel.contentMode = .bottom
        priceLabel.sizeToFit()
        previousPriceLabel = UILabel()
        previousPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(previousPriceLabel)
        previousPriceLabel.layoutAttachTrailing(margin: 16)
        previousPriceLabel.leadingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 12).isActive = true
        previousPriceLabel.bottomAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 0).isActive = true
    }
    
    public override func configureCell(item: DBProductPriceItem) {
        self.priceLabel.textColor = UIColor.black
        self.previousPriceLabel.attributedText = nil
        
        self.priceLabel.text = DBStringUtil.convertStringValue(i: item.price)
        
        if (item.previousPrice != nil) {
            previousPriceLabel.attributedText = DBStringUtil.makeStrikePrice(price: item.previousPrice!)
            self.priceLabel.textColor = UIColor.red
        }
        
        print("\(priceLabel.intrinsicContentSize)")
        print("\(priceLabel.frame)")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


