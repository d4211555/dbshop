//
//  ProductGalleryHorizontalCell.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 12.05.2021.
//  Copyright © 2021 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import DBTableCollectionView
import UIKit
import SDWebImage

public class DBProductGalleryHorizontalCell<Model: DBImageItem>: DBHorizontalTableViewCell<DBHorizontalItem, DBProtocol>, DBCollectionViewAdapterDelegate  {
    
    public class DBGallerySection: DBCollectionViewSection {
        
        public override func getConfiguration(isHeader: Bool = false) {
            self.setConfigutarion(itemSpacing: 0, lineSpacing: 0, groupSpacing: 0, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), widthRatio: 1.0, heightRatio: self.heightRatio, itemCount: 1, scrollBehavior: .paging)
        }
//        public override func getHorizontalConfiguration() -> (itemSpacing: CGFloat, lineSpacing: CGFloat, sectionInset: UIEdgeInsets)? {
//            return (0.0, 0.0, UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0))
//        }
    }
    
    var pageControl: UIPageControl!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addPageControl()
        self.selectionStyle = .none
        collectionView.backgroundColor = UIColor.white
        self.backgroundColor = UIColor.white
        collectionView.isPagingEnabled = true
        collectionView.collectionViewDelegate = self
        
        print("\(collectionView.contentInset)")
    }
    
    func addPageControl() {
        pageControl = UIPageControl(frame: CGRect.zero)
        pageControl.numberOfPages = 1
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.black
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(pageControl)
        pageControl.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
        pageControl.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 0).isActive = true
        
    }
    
    @available(iOS 13.0, *)
    public func visibleItemsInvalidation(visibleItems: [NSCollectionLayoutVisibleItem]) {
        pageControl.currentPage = visibleItems.last!.indexPath.row
    }
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let w = scrollView.bounds.size.width
        let currentPage = Int(ceil(x/w))
        if (0.0 != fmodf(Float(currentPage), 1.0)){
            pageControl.currentPage = currentPage + 1;
        }else{
            pageControl.currentPage = currentPage;
        }
    }
    
    public override func configureCell(item: DBHorizontalItem) {
        super.configureCell(item: item)
        pageControl.numberOfPages = item.getItemCount()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}

public protocol DBGalleryTableCellDelegate: DBProtocol {
    func cellDidSelect(item: DBImageItem, image: UIImage)
}

open class DBGalleryTableCell<Model: DBImageItem>: DBCollectionViewCell<Model, DBGalleryTableCellDelegate> {
    public var imageView: UIImageView!
    public var imageViewContentMode: ContentMode = .scaleAspectFill
    
    public class func getConfiguration(delegate: DBGalleryTableCellDelegate?) -> DBCollectionViewCellConfig {
        return super.getConfiguration(delegate: delegate, nibName: nil, registerType: .registerClass)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        imageView = UIImageView()
        imageView.contentMode = imageViewContentMode
        imageView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(imageView)
        imageView.layoutAttachAll()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func cellDidSelect(item: Model) {
        getDelegate()?.cellDidSelect(item: item, image: self.imageView.image!)
    }
    
    open override func configureCell(item: Model) {
        imageView.sd_setImage(with: item.getImage(),
                              placeholderImage: UIImage(),
                              options: .refreshCached)
        print("GalleryCell cell rect - \(frame)")
    }
}

open class DBImageItem: DBModel {
    
    public var id : Int!
    
    public init(id: Int) {
        self.id = id
    }
    
    open func getImage() -> URL{
        return URL(string: "")!
    }
    
    
}

