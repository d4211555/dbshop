//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 22.08.2021.
//

import Foundation
import DBTableCollectionView
import UIKit

public class DBShortReviewItem: DBModel {
    var value: Double!
    var reviewsCount: Int!
    
    public init(value: Double, reviewsCount: Int) {
        self.value = value
        self.reviewsCount = reviewsCount
    }
}

public protocol DBShortReviewTableViewCellDegelate: DBProtocol {
    func cellDidSelect(item: DBShortReviewItem)
}

public class DBShortReviewTableViewCell: DBTableViewCell<DBShortReviewItem, DBShortReviewTableViewCellDegelate> {
     
    var reviewView: DBFloatRatingView!
    var reviewsCountLabel: UILabel!
    var reviewDetailLabel: UILabel!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        reviewView = DBFloatRatingView(frame: CGRect.zero)
        reviewView.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(reviewView)
        reviewView.layoutAttachTop(margin: 8)
        reviewView.layoutAttachLeading(margin: 16)
        reviewView.widthAnchor.constraint(equalTo: self.contentView.widthAnchor, multiplier: 0.3, constant: 0).isActive = true
        reviewView.heightAnchor.constraint(equalToConstant: 20).isActive = true
        reviewView.isUserInteractionEnabled = false
        reviewDetailLabel = UILabel()
        reviewDetailLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(reviewDetailLabel)
        reviewDetailLabel.leadingAnchor.constraint(equalTo: self.reviewView.trailingAnchor, constant: 8).isActive = true
        reviewDetailLabel.centerYAnchor.constraint(equalTo: reviewView.centerYAnchor).isActive = true
        reviewDetailLabel.font = UIFont.systemFont(ofSize: 14)
        
        reviewsCountLabel = UILabel()
        reviewsCountLabel.translatesAutoresizingMaskIntoConstraints = false
        reviewsCountLabel.font = UIFont.systemFont(ofSize: 14)
        reviewsCountLabel.textColor = UIColor.blue
        self.contentView.addSubview(reviewsCountLabel)
        reviewsCountLabel.topAnchor.constraint(equalTo: reviewView.bottomAnchor, constant: 8).isActive = true
        reviewsCountLabel.layoutAttachLeading(margin: 16)
        reviewsCountLabel.layoutAttachBottom(margin: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func cellDidSelect(item: DBShortReviewItem) {
        getDelegate()?.cellDidSelect(item: item)
    }
    
    public class func getConfiguration(delegate: DBShortReviewTableViewCellDegelate?) -> DBTableViewCellConfig{
        return DBTableViewCellConfig(type: DBShortReviewTableViewCell.self, cellIdentifier: "DBShortReviewTableViewCell", delegate: delegate, modelType: DBShortReviewItem.self, registerType: .registerClass, nibName: nil)
    }
    
    public override func configureCell(item: DBShortReviewItem) {
        reviewView.rating = item.value
        var string = "отзывов"
        if (item.reviewsCount == 1) {
            string = "отзыв"
        }else if (item.reviewsCount > 1 && item.reviewsCount < 5) {
            string = "отзыва"
        }
        reviewsCountLabel.text = "\(item.reviewsCount!) \(string)"
        reviewDetailLabel.text = "\(IntegerUtil.decimalValue(value: item.value!))"
    }
}
