//
//  File.swift
//  
//
//  Created by Dmitrii on 27.01.2023.
//

import Foundation
import UIKit
import DBTableCollectionView

//
public class DBWhatsAppManagerItem: DBModel {

}

public protocol DBWhatsAppManagerTableViewCellDegelate: DBProtocol {
    func cellDidSelect(item: DBWhatsAppManagerItem)
}

public class DBWhatsAppManagerTableViewCell: DBTableViewCell<DBWhatsAppManagerItem, DBWhatsAppManagerTableViewCellDegelate> {
    
    public class func getConfiguration(delegate: DBWhatsAppManagerTableViewCellDegelate?) -> DBTableViewCellConfig{
        return getConfiguration(delegate: delegate, nibName: nil, registerType: .registerClass)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        self.separatorInset = UIEdgeInsets(top: 0, left: 1000, bottom: 0, right: 0)
        let im = UIImageView()
        im.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(im)
        im.layoutAttachTop(margin: 8)
        im.layoutAttachBottom(margin: 8)
        im.layoutAttachLeading(margin: 16)
        im.layoutAttachHeight(constant: 40)
        im.layoutAttachWidth(constant: 40)
        im.image = UIImage(named: "whatsapp", in: Bundle.module, compatibleWith: nil)!
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(label)
        label.layoutAttachTop(margin: 8)
        label.layoutAttachBottom(margin: 8)
        label.layoutAttachLeading(to: im, margin: 16)
        label.layoutAttachTrailing(margin: 16)
        label.text = DBStrings.askManager
        label.textColor = DBColors.primaryColor
        label.font = UIFont.boldSystemFont(ofSize: 16)
    }
    
    public override func configureCell(item: DBWhatsAppManagerItem) {
        
    }
    
    public override func cellDidSelect(item: DBWhatsAppManagerItem) {
        getDelegate()?.cellDidSelect(item: item)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


