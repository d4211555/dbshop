//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 03.06.2021.
//

import Foundation
import DBTableCollectionView
import UIKit

public class DBTitledTextLabelItem: DBModel {
    var text: String!
    var title: String!
    var textLabelInteractionEnabled: Bool!
    public init(text: String, title: String, textLabelInteractionEnabled: Bool = false) {
        self.text = text
        self.title = title
        self.textLabelInteractionEnabled = textLabelInteractionEnabled
    }
}

public class DBTitledTextLabelTableViewCell: DBTableViewCell<DBTitledTextLabelItem, DBProtocol> {
     
    var titleLabel: UILabel!
    var label: DBTextViewLikeLabel!
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 0
        contentView.addSubview(titleLabel)
        titleLabel.layoutAttachTop(margin: 12)
        titleLabel.layoutAttachLeading(margin: 16)
        titleLabel.layoutAttachTrailing(margin: 16)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        label = DBTextViewLikeLabel()
        contentView.addSubview(label)
        label.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 12).isActive = true
        label.layoutAttachBottom(margin: 12)
        label.layoutAttachLeading(margin: 16)
        label.layoutAttachTrailing(margin: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public class func getConfiguration(delegate: DBProtocol?) -> DBTableViewCellConfig{
        return DBTableViewCellConfig(type: DBTitledTextLabelTableViewCell.self, cellIdentifier: "DBTitledTextLabelTableViewCell", delegate: delegate, modelType: DBTitledTextLabelItem.self, registerType: .registerClass, nibName: nil)
    }
    
    public override func configureCell(item: DBTitledTextLabelItem) {
        titleLabel.text = item.title
        label?.text = item.text
        
        label?.isUserInteractionEnabled = item.textLabelInteractionEnabled
    }
}
