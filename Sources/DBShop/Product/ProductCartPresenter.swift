//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 03.06.2021.
//

import Foundation

public protocol DBCartLoadingView: DBLoadingView {
    associatedtype ProductT: DBProduct
    func productAddedToCart(product: ProductT)
    func productAlreadyAtCart()
    func productRemovedFromCart(product: ProductT)
    func productAddedToFavourite(product: ProductT)
    func productRemovedFromFavourite(product: ProductT)
    func allProductsRemovedFromCart()
}


open class DBProductCartLoadingPresenter<T: DBCartLoadingView, CoreDataCart: DBCoreDataCartProtocol, CoreDataFavourite: DBCoreDataFavouriteProtocol, M: DBProduct, CartItem: DBCartItemModel>: DBLoadingPresenter<T>{
    
    public var products: [M]? {
        didSet {
            self.validateCartProducts(products: products)
        }
    }
    
    public var product: M?  {
        didSet {
            self.validateCartProduct(product: product)
        }
    }
    
    internal var coreDataCartManager: CoreDataCart? = nil
    internal var coreDataFavouriteManager: CoreDataFavourite? = nil
    
    public init(view: T?, coreDataCartManager: CoreDataCart?, coreDataFavouriteManager: CoreDataFavourite?, coreDataAuthManager: DBCoreDataAuthProtocol?) {
        super.init(view: view, coreDataAuthManager: coreDataAuthManager)
        self.coreDataCartManager = coreDataCartManager
        self.coreDataFavouriteManager = coreDataFavouriteManager
    }
    
    /**
     При старте дочерний презентер подписывается на натификации о добавлении продуктов
     */
    
    override open func start() {
        super.start()
        DBAppLocalNotification.addLocalSubscriber(key: getClassName()+"_DBProductCartLoadingPresenter", localEventType: [.productAddedToCart, .productRemovedFromCart, .allProductRemovedFromCart, .productAddedToFavourite, .productRemovedFromFavourite, ]) { (localEvent) in
            
            if (localEvent.type == DBChangesNotificationType.allProductRemovedFromCart) {
                self.products?.forEach{$0.isInCart = false}
                self.product?.isInCart = false
                self.view?.allProductsRemovedFromCart()
                return
            }
            
            guard let productId = localEvent.objectId else { return }
           // guard let product = self.findProduct(productId: productId).is else { return }
            self.findProduct(productId: productId)?.forEach({ product in
                if (localEvent.type == DBChangesNotificationType.productAddedToCart) {
                    product.isInCart = true
                    self.view?.productAddedToCart(product: product as! T.ProductT)
                }
                if (localEvent.type == DBChangesNotificationType.productRemovedFromCart) {
                    product.isInCart = false
                    self.view?.productRemovedFromCart(product: product as! T.ProductT)
                }
                if (localEvent.type == DBChangesNotificationType.productAddedToFavourite) {
                    product.isInFavourite = true
                    self.view?.productAddedToFavourite(product: product as! T.ProductT)
                }
                if (localEvent.type == DBChangesNotificationType.productRemovedFromFavourite) {
                    product.isInFavourite = false
                    self.view?.productRemovedFromFavourite(product: product as! T.ProductT)
                }
            })
           
        }
    }
    
    func findProduct(productId: Int) -> [M]?{
        if (products != nil) {
            return products?.filter{$0.getProductIdValue() == productId}
        }
        if (product?.getProductIdValue() == productId) {
            return [product!]
        }
        return nil
    }
    
    public func validateCartProducts(products: [DBProduct]?) {
        if let productsInCart = coreDataCartManager?.getAllItems().map({$0.productId}) {
            products?.forEach({ (product) in
                let s = product.getProductIdValue()
                product.isInCart = productsInCart.contains(where: {$0 == s})
            })
        }
         
        if let productsInFavourite = coreDataFavouriteManager?.getAllFavouriteItems().map({$0.productId}) {
            products?.forEach({ (product) in
                let s = product.getProductIdValue()
                product.isInFavourite = productsInFavourite.contains(where: {$0 == s})
            })
        }
    }
    
    public func validateCartProduct(product: DBProduct?) {
        if let productsInCart = coreDataCartManager?.getAllItems().map({$0.productId}) {
            product?.isInCart = productsInCart.contains(where: {$0 == product?.getProductIdValue()})
        }
         
        if let productsInFavourite = coreDataFavouriteManager?.getAllFavouriteItems().map({$0.productId}) {
            product?.isInFavourite = productsInFavourite.contains(where: {$0 == product?.getProductIdValue()})
        }
    }
    
    public func addToBucket(product: M, count: Int) {
        let s = product.getProductIdValue()
        if coreDataCartManager?.getItem(productId: s ) == nil {
            if coreDataCartManager?.addItem(productId: s , count: count) == true {
                DBRuntimeStorageSingleton.sharedInstance.addCartItem(productId: s , count: count)
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateCart)
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateOrderBadge)
                DBAppLocalNotification.sendLocalNotification(notificationType: .productAddedToCart, objectId: product.getProductIdValue())
            }
        }else{
            self.view?.productAlreadyAtCart()
        }
    }
    
    public func addToBucket(item: CartItem) {
        let s = item.productId
        if coreDataCartManager?.getItem(productId: s!) == nil {
            if coreDataCartManager?.addItem(item: item as! CoreDataCart.T) == true {
                DBRuntimeStorageSingleton.sharedInstance.addCartItem(productId: s! , count: item.count)
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateCart)
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateOrderBadge)
                DBAppLocalNotification.sendLocalNotification(notificationType: .productAddedToCart, objectId: s)
            }
        }else{
            self.view?.productAlreadyAtCart()
        }
    }
    
    public func addToFavourite(product: M) {
        let s = product.getProductIdValue()
        if coreDataFavouriteManager?.getFavouriteItem(productId: s) == nil {
            if coreDataFavouriteManager?.addFavouriteItem(productId: s) == true {
                DBRuntimeStorageSingleton.sharedInstance.addFavouriteItem(productId: s)
                DBAppLocalNotification.sendLocalNotification(notificationType: .productAddedToFavourite, objectId: product.getProductIdValue())
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateFavouriteList)
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateFavouritesCount)
            }
        }
    }
    
    public func removeFromFavourite(product: M) {
        let s = product.getProductIdValue()
        if coreDataFavouriteManager?.deleteFavouriteItem(productId: s) == true {
            DBRuntimeStorageSingleton.sharedInstance.removeFavouriteItem(productId: s)
            DBAppLocalNotification.sendLocalNotification(notificationType: .productRemovedFromFavourite, objectId: product.getProductIdValue())
            DBAppLocalNotification.sendLocalNotification(notificationType: .updateFavouriteList)
            DBAppLocalNotification.sendLocalNotification(notificationType: .updateFavouritesCount)
        }
    }
  
    open override func stop() {
        super.stop()
        DBAppLocalNotification.removeLocalSubscriber(key: getClassName()+"_DBProductCartLoadingPresenter")
    }
    
    open func setFavourite(product: M) {
        if (product.isInFavourite) {
            removeFromFavourite(product: product)
        }else{
            addToFavourite(product: product)
        }
    }
    
}
