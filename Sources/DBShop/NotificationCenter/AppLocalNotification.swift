//
//  AppLocalNotification.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 24.06.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import RxSwift


open class DBAppLocalNotification {
    
    private static let localSubject = PublishSubject<DBLocalEvent>()
    static var localDisposableMap = Dictionary<String, Disposable>()
    
    public class func addLocalSubscriber(key: String, localEventType: [DBChangesNotificationType], onNext: ((DBLocalEvent) -> Void)?) {
        
        localDisposableMap[key]?.dispose()
        localDisposableMap[key] = localSubject
            .filter({ (localEvent) -> Bool in
                return localEventType.contains(localEvent.type)
            })
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: onNext, onError: { (error) in
                print("AppLocalNotification error - \(error)")
            }, onCompleted: {
                
            }, onDisposed: {
                
            })
//        print("localDisposableMap add key = \(key)")
//        localDisposableMap.map({ (key1: String, value: Disposable) in
//            print("localDisposableMap key = \(key1)")
//        })
    }
    
    public class func sendLocalNotification(type: DBLocalEvent) {
        localSubject.onNext(type)
    }
    
    public class func sendLocalNotification(notificationType: DBChangesNotificationType) {
        let localEvent = DBLocalEvent()
        localEvent.type = notificationType
        localSubject.onNext(localEvent)
    }
    
    public class func sendLocalNotification(notificationType: DBChangesNotificationType, objectId: Int?) {
        let localEvent = DBLocalEvent()
        localEvent.objectId = objectId
        localEvent.type = notificationType
        localSubject.onNext(localEvent)
    }
    
    public class func removeLocalSubscriber(key: String) {
        localDisposableMap.removeValue(forKey: key)?.dispose()
    }
    
}

open class DBLocalEvent{
    public var type: DBChangesNotificationType!
    var events: [DBRuntimeEvent]?
    var objectId: Int?
}

public enum DBChangesNotificationType {
    case updateEventsCount
    case updateEvents
    case updateOrderBadge
    case updateCart
    case productRemovedFromCart
    case productAddedToCart
    case allProductRemovedFromCart
    case productAddedToFavourite
    case productRemovedFromFavourite
    case updateUserCabinet
    case updateFavouriteList
    case updateFavouritesCount
    case refreshScreen
    case refreshSeminarsListScreen
    case orderPaymentCallback
}


public class DBRuntimeStorageSingleton {
    
    public static let sharedInstance = DBRuntimeStorageSingleton()
    var cartItems = Dictionary<Int, Int>()
    var favouriteItems = Set<Int>()
    var runtimeEvents = [DBRuntimeEvent]()
    
    public func addCartItem(productId: Int, count: Int) {
        cartItems.updateValue(count, forKey: productId)
    }
    
    public func removeCartItem(productId: Int) {
        cartItems.removeValue(forKey: productId)
    }
    
    public func removeAllCartItems() {
        cartItems.removeAll()
    }
    
    public func getCartItemsCount() -> Int {
        let b = cartItems.compactMap { $0.value }.reduce(0, +)
        return b
    }
    
    //MARK:- Events
    
    public func setRuntimeEvents(runtimeEvents: [DBRuntimeEvent]) {
        self.runtimeEvents = runtimeEvents
    }
    
    public func getRuntimeEventsCount() -> Int {
        return runtimeEvents.count
    }
    
    public func removeRuntimeEvent(_ id: Int) {
        runtimeEvents.removeAll { (value) -> Bool in
            if value.id == id {
                return true
            }else{
                return false
            }
        }
    }
    
    public func removeRuntimeEventsByObjectId(_ objectId: Int) {
        runtimeEvents.removeAll { (value) -> Bool in
            if value.objectId == objectId {
                return true
            }else{
                return false
            }
        }
    }
    
    //MARK:- Favourites
    public func getRuntimeFavouriteCount() -> Int {
        return favouriteItems.count
    }
    
    public func setRuntimeFavourites(favouriteItems: [DBFavouriteItemModel]) {
        self.favouriteItems = Set(favouriteItems.map{$0.productId})
    }
    
    public func addFavouriteItem(productId: Int) {
        favouriteItems.insert(productId)
    }
    
    public func removeFavouriteItem(productId: Int) {
        favouriteItems.remove(productId)
    }
    
    
}


open class DBRuntimeEvent{
    public var id: Int!
    public var objectId: Int?
    public var read: Bool = false
    
    public init(id: Int, objectId: Int?, read: Bool = false) {
        self.id = id
        self.read = read
        self.objectId = objectId
    }
}
