//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 28.05.2021.
//

import Foundation

open class DBPushBroadcast {
    
    public class func convertToDictionary(text: String) -> [Dictionary<String, AnyObject>]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [Dictionary<String, AnyObject>]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
}
