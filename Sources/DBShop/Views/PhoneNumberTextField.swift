//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 13.07.2021.
//

import Foundation
import PhoneNumberKit

open class DBPhoneNumberTextField: PhoneNumberTextField {
    open override var defaultRegion: String {
            get {
                return "RU"
            }
            set {} // exists for backward compatibility
        }
    open override func awakeFromNib() {
        super.awakeFromNib()
        self.withPrefix = true
        self.withExamplePlaceholder = true
    }
}

