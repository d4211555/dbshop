//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 11.08.2021.
//

import Foundation
import UIKit

open class DBImage {
    
    public class func avatarPlaceholder() -> UIImage {
        return UIImage(named: "profile_placeholder", in: Bundle.module, compatibleWith: nil)!
    }
    
    public class func info() -> UIImage {
        //UIImage(imageLiteralResourceName: "myImageName")
        return UIImage(named: "ic_info", in: Bundle.module, compatibleWith: nil)!.withRenderingMode(.alwaysTemplate)
    }
    
    public class func sale() -> UIImage {
        return UIImage(named: "ic_sale", in: Bundle.module, compatibleWith: nil)!
    }
    
    public class func emptyStar() -> UIImage {
        return UIImage(named: "ic_empty_star", in: Bundle.module, compatibleWith: nil)!
    }
    
    public class func fullStar() -> UIImage {
        return UIImage(named: "ic_full_star", in: Bundle.module, compatibleWith: nil)!
    }
    
    public class func favouriteEmpty() -> UIImage {
        return UIImage(named: "ic_favourite_empty", in: Bundle.module, compatibleWith: nil)!
    }
    
    public class func favouriteFilled() -> UIImage {
        return UIImage(named: "ic_favourite_filled", in: Bundle.module, compatibleWith: nil)!
    }
}
