//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 06.08.2021.
//

import Foundation
import UIKit

public protocol DBDropDownListTitle {
    func getTitle() -> String
}

public class DBDropDownList {
    public class func open<T: DBDropDownListTitle>(title: String, vc: UIViewController, list: [T], completion: @escaping (_ selected: T) -> ()){
        UILabel.appearance(whenContainedInInstancesOf: [UIAlertController.self]).numberOfLines = 2
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
        for i in 0...list.count - 1 {
            let action = DBCustomAlertAction(title: list[i].getTitle(), style: .default, handler: { (action) in
                let index = list[(action as! DBCustomAlertAction).tag]
                completion(index)
            })
            action.tag = i
            alert.addAction(action)

        }
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        vc.present(alert, animated: true, completion: nil)

    }
}

public class DBDropDownObject: DBDropDownListTitle {
    var id: Int!
    var title: String!
    
    public init(id: Int, title: String) {
        self.id = id
        self.title = title
    }
    
    public func getTitle() -> String {
        return title
    }
    
    public func getId() -> Int {
        return id
    }
}

class DBCustomAlertAction: UIAlertAction {
    var tag: Int!
}
