//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 12.08.2021.
//

import Foundation
import UIKit

public class DBAlert {
    public class func cameraOrLibrary(vc: UIViewController, cameraAction: @escaping ()->Void, libraryAction: @escaping ()->Void ) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Сделать снимок" , style: .default, handler: { (action) -> Void in
            cameraAction()
        }))
        alert.addAction(UIAlertAction(title: "Выбрать из библиотеки", style: .default, handler: { (action) -> Void in
            libraryAction()
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (action) -> Void in
        }))
        vc.present(alert, animated: true) { () -> Void in
        }
    }
    
    public class func deleteObject(vc: UIViewController, action: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите удалить?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: action))
        alert.addAction(UIAlertAction(title: "Нет", style: .cancel, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    public class func accept(vc: UIViewController, title: String? = nil, message: String, noAction: ((UIAlertAction) -> Void)? = nil, action: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Да", style: .default, handler: action))
        alert.addAction(UIAlertAction(title: "Нет", style: .cancel, handler: noAction))
        vc.present(alert, animated: true, completion: nil)
    }
    
    public class func ok(vc: UIViewController, title: String?, message: String, action: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: action))
        vc.present(alert, animated: true, completion: nil)
    }
    
    public class func buttonWithCancel(vc: UIViewController, title: String?, message: String, buttonTitle: String, buttonAction: @escaping ((UIAlertAction) -> Void), cancelAction: @escaping ((UIAlertAction) -> Void)) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: buttonAction))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: cancelAction))
        vc.present(alert, animated: true, completion: nil)
    }
   

    /**
        Alert blabla bla
     */
    public class func open(vc: UIViewController, title: String?, message: String?, style: UIAlertController.Style, showCancel: Bool = true, actions: [UIAlertAction], textFields: [(UITextField) -> Void]?) {
        var actions = actions
        let alertController = UIAlertController(title: title, message: message, preferredStyle: style)
        textFields?.forEach{alertController.addTextField(configurationHandler: $0)}
        if (showCancel) {
            actions.append(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        }
        actions.forEach{alertController.addAction($0)}
        vc.present(alertController, animated: true, completion: nil)
    }
}
