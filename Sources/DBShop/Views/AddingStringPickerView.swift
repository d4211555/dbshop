//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 06.08.2021.
//

import Foundation
import UIKit

public class DBAddingStringPickerView<Button: DBButton>: UIView {
    
    //private var content: [String]!
    public var onButtonTapped : (()->Void)!
    public var onSelect : ((_ date: String)->Void)?
    
    public init(_ content: [String], selectedText: String?, buttonTitle: String) {
        //self.content = content
        super.init(frame: CGRect.zero)
        
        let pickerView = DBStringPickerView(content, selectedText)
        pickerView.onSelect = { [unowned self] date in
            self.onSelect?(date)
        }
        self.addSubview(pickerView)
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        pickerView.layoutAttachTop(to: nil, margin: 8)
        pickerView.layoutAttachTrailing()
        pickerView.layoutAttachLeading()
        
        
        let button = Button()
        button.primary()
        button.setTitle(buttonTitle, for: .normal)
        self.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layoutAttachTrailing(margin: 16)
        button.layoutAttachLeading(margin: 16)
        button.layoutAttachBottom(margin: 16)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.topAnchor.constraint(equalTo: pickerView.bottomAnchor, constant: 8).isActive = true
        button.addAction { [unowned self] in
            self.onButtonTapped()
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    deinit {
        print("DBAddingStringPickerView deinit")
    }
}
