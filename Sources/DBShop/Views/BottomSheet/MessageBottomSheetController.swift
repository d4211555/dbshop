//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 16.08.2021.
//

import Foundation
import UIKit

public class DBMessageBottomSheetController: DBBottomSheetViewController {
    
    public init(title: String, message: String) {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = message
        
        super.init(contentView: label, title: title, top: 16, right: 16, left: 16, bottom: 16)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
