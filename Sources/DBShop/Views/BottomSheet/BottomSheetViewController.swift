//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 04.08.2021.
//

import Foundation
import UIKit

public class DBBottomSheetViewController: UIViewController {
    lazy var backdropView: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        return bdView
    }()
    
    lazy var closeButtonView: UIView = {
        let closeButtonView = UIView(frame: self.view.bounds)
        let button = DBViewHelper.getCloseButton()
        button.addAction { [unowned self] in
            self.dismiss()
        }
        closeButtonView.addSubview(button)
        
        let title = UILabel()
        title.tag = 100
        title.text = " "
        title.numberOfLines = 0
        title.font = UIFont.boldSystemFont(ofSize: 21)
        closeButtonView.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.topAnchor.constraint(equalTo: closeButtonView.topAnchor, constant: 16).isActive = true
        title.leadingAnchor.constraint(equalTo: closeButtonView.leadingAnchor, constant: 16).isActive = true
        title.bottomAnchor.constraint(equalTo: closeButtonView.bottomAnchor, constant: -8).isActive = true
        title.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
        
        
        
        button.topAnchor.constraint(equalTo: closeButtonView.topAnchor, constant: 16).isActive = true
        button.trailingAnchor.constraint(equalTo: closeButtonView.trailingAnchor, constant: -16).isActive = true
        button.leadingAnchor.constraint(equalTo: title.trailingAnchor, constant: 16).isActive = true

        closeButtonView.backgroundColor = UIColor.white
        return closeButtonView
    }()
    
    public var onDismiss : (()->Void)?
    
    var menuView: UIView!
    
    lazy var containerView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.white
        return v
    }()
    
    let menuHeight = UIScreen.main.bounds.height / 2
    var isPresenting = false
    var top: CGFloat!
    var right: CGFloat!
    var left: CGFloat!
    var bottom: CGFloat!
    var titleString: String?
    
    public init(contentView: UIView, title: String? = nil, top: CGFloat = 0, right: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0) {
        super.init(nibName: nil, bundle: nil)
        self.menuView = contentView
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right
        self.titleString = title
        modalPresentationStyle = .custom
        transitioningDelegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    public override func show(_ vc: UIViewController, sender: Any?) {
        self.modalPresentationStyle = .custom
        vc.present(self, animated: true, completion: nil)
    }
    
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        containerView.roundCorners(corners: [.topLeft, .topRight], radius: 10)
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .clear
        view.addSubview(backdropView)
        view.addSubview(containerView)
        containerView.addSubview(menuView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
//        if #available(iOS 11.0, *) {
//            let guide = view.safeAreaLayoutGuide
//            let height = guide.layoutFrame.size.height
//            print(height)
//        } else {
//            // Fallback on earlier versions
//        }
        
        containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true

        containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        containerView.addSubview(closeButtonView)
        closeButtonView.translatesAutoresizingMaskIntoConstraints = false
        closeButtonView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0).isActive = true
        closeButtonView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: 0).isActive = true
        closeButtonView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 0).isActive = true
        
        
        menuView.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            menuView.bottomAnchor.constraint(equalTo: containerView.safeAreaLayoutGuide.bottomAnchor, constant: -bottom).isActive = true
        } else {
            menuView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -bottom).isActive = true
        }
        menuView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: left).isActive = true
        menuView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -right).isActive = true
        menuView.topAnchor.constraint(equalTo: closeButtonView.bottomAnchor, constant: top).isActive = true
        //menuView.clipsToBounds = true
        
        
        if (titleString != nil) {
            (closeButtonView.viewWithTag(100) as? UILabel)?.text = titleString
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(DBBottomSheetViewController.handleTap(_:)))
        backdropView.addGestureRecognizer(tapGesture)
    }
    
    public func dismiss() {
        self.dismiss(animated: true) {
            self.onDismiss?()
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss()
    }
    
    deinit {
        print("DBBottomSheetViewController deinit")
    }
}

extension DBBottomSheetViewController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        
        if isPresenting == true {
            containerView.addSubview(toVC.view)
            
            self.containerView.frame.origin.y += menuHeight
            backdropView.alpha = 0
            
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
                self.containerView.frame.origin.y -= self.menuHeight
                self.backdropView.alpha = 1
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseOut], animations: {
                self.containerView.frame.origin.y += self.menuHeight
                self.backdropView.alpha = 0
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        }
    }
}

