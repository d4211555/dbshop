//
//  File.swift
//  
//
//  Created by Dmitrii on 04.08.2023.
//

import Foundation
import UIKit
import DBTableCollectionView

public class DBSize: DBModel, Codable {
    public var id: Int
    public var value: String
    public var available: DBProductAvailableType
}

public protocol DBSizesStackViewDelegate: AnyObject {
    func sizeSelected(size: DBSize)
}

open class DBSizesStackView: UIScrollView {
    
    weak public var customDelegate: DBSizesStackViewDelegate?
    var stackView = UIStackView()
    var sizes: [DBSize]!
    public init(frame: CGRect, height: CGFloat) {
        super.init(frame: frame)
        addStackView(height: height)
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        addStackView(height: nil)
    }
    
    
    func addStackView(height: CGFloat?) {
        self.backgroundColor = UIColor.white
        self.showsHorizontalScrollIndicator = false
        self.clipsToBounds = true
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = UIColor.white
        self.addSubview(stackView)
        stackView.layoutAttachAll()
        stackView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        if (height != nil) {
            stackView.heightAnchor.constraint(equalToConstant: height!).isActive = true
        }
        self.stackView.spacing = 4
    }
    
    public func configure(items: [DBSize], selectedSize: DBSize?, isSelected: Bool) {
       // let ii = items + items + items + items + items
        self.sizes = items
        self.stackView.removeAllSubviews()
        items.forEach { size in
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.cornerRadius = 10
            view.layer.borderWidth = 0.5
            view.layer.borderColor = DBColors.secondaryColor?.cgColor
            if (isSelected) {
                view.addAction { [unowned self] in
                    self.selectView(view: view)
                }
            }
            
            
            
            let label = UILabel()
            label.font = UIFont.systemFont(ofSize: 12)
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = size.value
            view.addSubview(label)
            label.adjustsFontSizeToFitWidth = true
            label.widthAnchor.constraint(greaterThanOrEqualTo: label.heightAnchor).isActive = true
            //label.widthAnchor.constraint(equalTo: label.heightAnchor, multiplier: 1).isActive = true
            
            label.layoutAttachAll(margin: 4)
            
            if (size.available == .notAvailable || size.available == .underTheOrder) {
                view.backgroundColor = DBColors.lightGrayColor
                view.layer.borderColor = UIColor.gray.cgColor
            }
            
            self.stackView.addArrangedSubview(view)
            
            if (selectedSize == size) {
                setSelected(view: view)
            }
        }
    }
    
    func selectView(view: UIView) {
        let size = self.sizes[self.stackView.arrangedSubviews.firstIndex(of: view)!]
        if (size.available == .notAvailable) {
            return
        }
        self.stackView.arrangedSubviews.forEach { v in
            setNormal(view: v)
        }
        setSelected(view: view)
        
        self.customDelegate?.sizeSelected(size: size)
        
    }
    
    func setNormal(view: UIView) {
        let size = self.sizes[self.stackView.arrangedSubviews.firstIndex(of: view)!]
        if (size.available == .notAvailable) {
            return
        }
        view.layer.borderColor = DBColors.secondaryColor?.cgColor
        view.backgroundColor = UIColor.white
        (view.subviews.first{$0 is UILabel} as! UILabel).textColor = UIColor.black
    }
    
    func setSelected(view: UIView) {
        view.backgroundColor = DBColors.secondaryColor
        view.layer.borderColor = UIColor.gray.cgColor
        (view.subviews.first{$0 is UILabel} as! UILabel).textColor = UIColor.white
    }
    
}

public class DBSizeItem: DBModel {
    var sizes: [DBSize]!
    public init(sizes: [DBSize]) {
        self.sizes = sizes
    }
}

public protocol DBSizeTableViewCellDelegate: DBProtocol {
    func sizeSelected(size: DBSize)
}

public class DBSizeTableViewCell: DBTableViewCell<DBSizeItem, DBSizeTableViewCellDelegate>, DBSizesStackViewDelegate {
     
    var titleLabel: UILabel!
    var sizesStackView: DBSizesStackView!
    var selectedSize: DBSize?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 0
        contentView.addSubview(titleLabel)
        titleLabel.layoutAttachTop(margin: 12)
        titleLabel.layoutAttachLeading(margin: 16)
        titleLabel.layoutAttachTrailing(margin: 16)
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        sizesStackView = DBSizesStackView(frame: CGRect.zero, height: 30)
        sizesStackView.customDelegate = self
        sizesStackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(sizesStackView)
        sizesStackView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 12).isActive = true
        sizesStackView.layoutAttachBottom(margin: 12)
        sizesStackView.layoutAttachLeading(margin: 16)
        sizesStackView.layoutAttachTrailing(margin: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public class func getConfiguration(delegate: DBSizeTableViewCellDelegate?) -> DBTableViewCellConfig{
        return DBTableViewCellConfig(type: DBSizeTableViewCell.self, cellIdentifier: "DBSizeTableViewCell", delegate: delegate, modelType: DBSizeItem.self, registerType: .registerClass, nibName: nil)
    }
    
    public func sizeSelected(size: DBSize) {
        self.selectedSize = size
        getDelegate()?.sizeSelected(size: size)
    }
    
    public override func configureCell(item: DBSizeItem) {
        titleLabel.text = "Размеры:"
        sizesStackView.configure(items: item.sizes, selectedSize: self.selectedSize, isSelected: true)
    }
}
