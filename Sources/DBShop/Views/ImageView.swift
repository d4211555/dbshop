//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 23.05.2022.
//

import Foundation
import UIKit
import SDWebImage

public class DBImageView: UIImageView {
    
    public func loadImage(url: URL?, placeholderImage: UIImage?, resizeImage: Bool = false, completedDownloading: (() -> ())? = nil) {
        //self.setNeedsLayout()
        if (resizeImage) {
            //SDImageCache.shared.clearMemory()
            //SDImageCache.shared.removeImageFromDisk(forKey: <#T##String?#>)
            self.sd_setImage(with: url, placeholderImage: placeholderImage, options: [.fromLoaderOnly], completed: {  [unowned self] ( image, error, cacheType, imageURL)  in
                //self.needsUpdateConstraints()
                if (image != nil) {
                    //self.image = image
                    print(self.frame)
                    let ratio = image!.size.width / image!.size.height
                    let newHeight = self.frame.width / ratio
                    self.layoutAttachHeight(constant: newHeight)
                }else{
                    self.image = nil
                }
                completedDownloading?()
            })
        }else{
            self.sd_setImage(with: url, placeholderImage: placeholderImage, options: .refreshCached)
        }
    }
    
    deinit {
        print("DBIMAGEVIEW DEINIT")
    }
    
}
