//
//  File 2.swift
//  
//
//  Created by Dmitry Bogomazov on 14.07.2021.
//

import Foundation
import UIKit

open class DBButton: UIButton {
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 5
        //self.titleLabel?.font = UIFont.systemFont(ofSize: 15)
    }
    
    required public init() {
        super.init(frame: CGRect.zero)
        self.awakeFromNib()
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        //fatalError("init(coder:) has not been implemented")
    }
    
    open func blueBG(){
        self.backgroundColor = DBColors.secondaryColor
        self.setTitleColor(UIColor.white, for: .normal)
    }
    
    open func primary(){
        self.backgroundColor = DBColors.primaryColor
        self.setTitleColor(DBColors.primaryButtonTextColor, for: .normal)
    }
    
    open func secondary(){
        self.backgroundColor = DBColors.secondaryColor
        self.setTitleColor(DBColors.secondaryButtonTextColor, for: .normal)
    }
    
    open func whiteBG(){
        self.backgroundColor = UIColor.white
        self.setTitleColor(DBColors.primaryColor, for: .normal)
    }
    
    open func greenBG(){
        self.backgroundColor = DBColors.greenColor
        self.setTitleColor(UIColor.white, for: .normal)
    }
    
    open func setNotEnabled() {
        self.isEnabled = false
        self.backgroundColor = DBColors.grayColor
        self.titleLabel?.textColor = UIColor.white
    }
    
    open func setEnabled() {
           self.isEnabled = true
           self.backgroundColor = DBColors.secondaryColor
        self.titleLabel?.textColor = DBColors.secondaryButtonTextColor
    }
    
    open func setPrimaryEnabled() {
           self.isEnabled = true
        self.primary()
    }
    
    open func setSecondaryEnabled() {
           self.isEnabled = true
        self.secondary()
    }

}
