//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 06.08.2021.
//

import Foundation
import UIKit

public class DBInsetLabel: UILabel {

    @IBInspectable
    public var topInset: CGFloat = 7
    
    @IBInspectable
    public var bottomInset: CGFloat = 7
    
    @IBInspectable
    public var leftInset: CGFloat = 7
    
    @IBInspectable
    public var rightInset: CGFloat = 7
    
    var prevPlaceholder: String?
    
    init(topInset: CGFloat = 7, bottomInset: CGFloat = 7, leftInset: CGFloat = 7, rightInset: CGFloat = 7) {
        super.init(frame: CGRect.zero)
        self.topInset = topInset
        self.bottomInset = topInset
        self.leftInset = topInset
        self.rightInset = topInset
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    
    public override func drawText(in rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
        self.numberOfLines = 0
        self.isUserInteractionEnabled = true
    }
    
    @IBInspectable
    public var placeholder: String? {
        didSet {
            text = placeholder
            prevPlaceholder = placeholder
            font = UIFont.systemFont(ofSize: 14)
            textColor = UIColor.lightGray.withAlphaComponent(0.7)
            
        }
    }
    
    
    
    public func setPreviousPlaceholder() {
        placeholder = prevPlaceholder
    }
    
    public func getText() -> String {
        if (text == placeholder) {
            return ""
        }
        return text
    }
    
    public func isEmptyText() -> Bool {
        if (text == placeholder || text == "") {
            return true
        }
        return false
    }
    
    public override var text: String! {
        didSet {
            font = UIFont.systemFont(ofSize: 14)
            textColor = UIColor.black
        }
    }
    
    override public var intrinsicContentSize: CGSize {
        var intrinsicSuperViewContentSize = super.intrinsicContentSize
        intrinsicSuperViewContentSize.height += topInset + bottomInset
        intrinsicSuperViewContentSize.width += leftInset + rightInset
        return intrinsicSuperViewContentSize
    }
}


