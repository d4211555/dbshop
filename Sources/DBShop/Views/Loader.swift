//
//  Loader.swift
//  CosmedexAdmin
//
//  Created by Dmitrii Bogomazov on 15.07.2018.
//  Copyright © 2018 Дима. All rights reserved.
//

import Foundation
import UIKit

class DBLoader {
    
    static var activityView : UIView?
    static var isLoading : Bool = false
    
    static func showLoader(view: UIView? = nil){
        let window = view == nil ? UIApplication.shared.keyWindow : view
        if activityView == nil {
            activityView = UIView(frame: window!.frame)
            activityView!.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
            let shadowView = UIView(frame: activityView!.frame)
            shadowView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
            activityView?.addSubview(shadowView)
            
            let indicatorView = UIView(frame: CGRect(x: activityView!.center.x - 35, y: activityView!.center.y - 35, width: 70, height: 70))
            indicatorView.backgroundColor = UIColor.black
            indicatorView.layer.cornerRadius = 20
            indicatorView.layer.masksToBounds = true
            
            let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
            activityIndicatorView.center = CGPoint(x: indicatorView.bounds.size.width/2, y: indicatorView.bounds.size.height/2)
            
            activityIndicatorView.color = UIColor.white
            
            indicatorView.addSubview(activityIndicatorView)
            indicatorView.bringSubviewToFront(activityIndicatorView)
            
            activityIndicatorView.startAnimating()
            activityView!.addSubview(indicatorView)
        }
        if !isLoading{
        window?.addSubview(activityView!)
            isLoading = true
        }
    }
    
    static func hideLoader(){
        if activityView != nil && isLoading == true{
            isLoading = false
            activityView!.removeFromSuperview()
        }
    }
}
