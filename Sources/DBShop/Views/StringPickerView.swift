//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 06.08.2021.
//

import Foundation
import UIKit

public class DBStringPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    public var onSelect : ((_ date: String)->Void)?
    private var content: [String]!
    private var selectedText: String?
    
    
    public init(_ content: [String], _ selectedText: String?) {
        self.content = content
        self.selectedText = selectedText
        super.init(frame: CGRect.zero)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        //onSelect?(content[0])
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        if (selectedText != nil) {
            if let i = content.firstIndex(of: selectedText!) {
                onSelect?(content[i])
                self.selectRow(i, inComponent: 0, animated: false )
                return
            }
        }
        onSelect?(content[0])
    }
    
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return content.count
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return content[row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        onSelect?(content[row])
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    
    deinit {
        print("deinit - DBStringPickerView")
    }
    
}

