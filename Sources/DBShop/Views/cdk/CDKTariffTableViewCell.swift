//
//  File.swift
//  
//
//  Created by Dmitrii on 05.09.2023.
//

import Foundation
import UIKit
import DBTableCollectionView

public class DBCDKTariffItem: DBModel {
    
    var name: String!
    var totalPrice: Int!
    var mailType: DBCDKTariffs!
    var deliveryTime: DBCDKGetTariffDeliveryTime?
    
    var isChose = false
    
    public init(name: String!, totalPrice: Int!, mailType: DBCDKTariffs!, deliveryTime: DBCDKGetTariffDeliveryTime? = nil) {
        self.name = name
        self.totalPrice = totalPrice
        self.mailType = mailType
        self.deliveryTime = deliveryTime
    }

}

public protocol DBCDKTariffTableViewCellDegelate: DBProtocol {
    func cellDidSelect(item: DBCDKTariffItem)
}

public class DBCDKTariffTableViewCell: DBTableViewCell<DBCDKTariffItem, DBCDKTariffTableViewCellDegelate> {
     
    var nameLabel: UILabel!
    var priceLabel: UILabel!
    var deliveryTimeLabel: UILabel!
    var chooseButton: DBButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        removeHeaderSeparator()
        nameLabel = UILabel(frame: CGRect.zero)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(nameLabel)
        nameLabel.layoutAttachTop(margin: 8)
        nameLabel.layoutAttachLeading(margin: 16)
        
        deliveryTimeLabel = UILabel(frame: CGRect.zero)
        deliveryTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        deliveryTimeLabel.font = UIFont.systemFont(ofSize: 10)
        deliveryTimeLabel.textColor = UIColor.lightGray
        
        self.contentView.addSubview(deliveryTimeLabel)
        deliveryTimeLabel.layoutAttachTop(to: nameLabel, margin: 4)
        deliveryTimeLabel.layoutAttachLeading(margin: 16)
        deliveryTimeLabel.layoutAttachBottom(margin: 8)
        
        chooseButton = DBButton()
        chooseButton.primary()
        chooseButton.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(chooseButton)
        //chooseButton.layoutAttachTop(margin: 8, relatedBy: .greaterThanOrEqual)
        //chooseButton.layoutAttachBottom(margin: 8, relatedBy: .greaterThanOrEqual)
        chooseButton.layoutAttachTrailing(margin: 16)
        chooseButton.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        chooseButton.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        chooseButton.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 0).isActive = true
        //chooseButton.titleLabel?.adjustsFontSizeToFitWidth = true
        chooseButton.titleLabel!.baselineAdjustment = .alignCenters
        //chooseButton.layoutAttachWidth(constant: 60, priority: UILayoutPriority(999))
        chooseButton.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        chooseButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        
        deliveryTimeLabel.layoutAttachTrailing(to: chooseButton, margin: 16)
        nameLabel.layoutAttachTrailing(to: chooseButton, margin: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func cellDidSelect(item: DBCDKTariffItem) {
        getDelegate()?.cellDidSelect(item: item)
    }
    
    public class func getConfiguration(delegate: DBCDKTariffTableViewCellDegelate?) -> DBTableViewCellConfig{
        
        return DBTableViewCellConfig(type: DBOrderShippingMethodTableViewCell.self, cellIdentifier: "DBCDKTariffTableViewCell", delegate: delegate, modelType: DBCDKTariffItem.self, registerType: .registerClass, nibName: nil)
    }
    
    public override func configureCell(item: DBCDKTariffItem) {
        self.nameLabel.text = item.name + " " + DBStringUtil.convertStringValue(i: item.totalPrice)
        self.setChooseButton()
        if (item.deliveryTime != nil) {
            self.deliveryTimeLabel.text = "от \(item.deliveryTime!.minDay) до \(item.deliveryTime!.maxDays) дней"
        }
        
        chooseButton.addAction { [unowned self] in
            self.item.isChose = true
            self.cellDidSelect(item: item)
        }
        
    }
    
    func setChooseButton() {
        if (self.item.isChose) {
            self.chooseButton.setTitle("Выбрано", for: .normal)
            self.chooseButton.greenBG()
        }else{
            self.chooseButton.setTitle("Выбрать", for: .normal)
            self.chooseButton.primary()
        }
        
    }
}
