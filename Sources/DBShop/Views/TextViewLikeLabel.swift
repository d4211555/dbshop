//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 11.04.2022.
//

import Foundation
import UIKit

class DBTextViewLikeLabel: UITextView {
    
    public init() {
        super.init(frame: CGRect.zero, textContainer: nil)
        sharedInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        sharedInit()
    }
    
    private func sharedInit() {
        self.textContainerInset = .zero
        self.textContainer.lineFragmentPadding = 0
        self.translatesAutoresizingMaskIntoConstraints = false
        self.sizeToFit()
        self.isEditable = false
        self.isScrollEnabled = false
        self.font = UIFont.systemFont(ofSize: 18)
        
    }
}
