//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 06.08.2021.
//

import Foundation
import UIKit
import DBTableCollectionView

public protocol DBAddressTableViewItemProtocol {
    var value: String! { get set }
}

public class DBAddressTableView<Button: DBButton>: UIView, DBTextLabelTableViewCellDelegate{

    public var onButtonTapped : (()->Void)!
    public var onSelect : ((_ address: DBAddressTableViewItemProtocol)->Void)?
    var content: [DBAddressTableViewItemProtocol]!
    var tableViewHeightConstraint: NSLayoutConstraint!
    fileprivate var tableView: DBSelfSizingTableView!
    
    public init(_ content: [DBAddressTableViewItemProtocol], buttonTitle: String) {
        super.init(frame: CGRect.zero)
        self.content = content
        tableView = DBSelfSizingTableView(frame: CGRect.zero, style: .grouped)
        tableView.maxCell = 3
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(tableView)
        tableView.layoutAttachTop(margin: 8)
        tableView.layoutAttachLeading(margin: 0)
        tableView.layoutAttachTrailing(margin: 0)
        //tableViewHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 100)
        //tableViewHeightConstraint.isActive = true
        tableView.backgroundColor = DBVariables.tableBackgroundColor
        tableView.removeTopSpace()
        tableView.removeBottomSpace()
        
        let button = Button()
        button.primary()
        button.setTitle(buttonTitle, for: .normal)
        self.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layoutAttachTrailing(margin: 16)
        button.layoutAttachLeading(margin: 16)
        button.layoutAttachBottom(margin: 16)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.topAnchor.constraint(equalTo: tableView.bottomAnchor, constant: 8).isActive = true
        button.addAction { [unowned self] in
            self.onButtonTapped()
        }
        
        
        
        tableView.registerCell(cell: DBTextLabelTableViewCell.getConfiguration(delegate: self))
        var array = [DBTextLabelItem]()
        content.forEach{array.append(DBTextLabelItem(text: $0.value, accessoryType: .disclosureIndicator, isSeparator: true, selectionStyle: .none))}
        tableView.add(section: DBTableViewSection(items: array))
        
        
    }
    
    public func accessoryButtonTapped(item: DBTextLabelItem){}
    public func cellDidSelect(item: DBTextLabelItem) {
        self.onSelect?(self.content.first{$0.value == item.text}!)
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("DBAddressTableView deinit")
    }
    
}

public class DBSelfSizingTableView: DBTableView {
    
    public var maxCell = 10
    
    public override var contentSize: CGSize {
        didSet {
            invalidateIntrinsicContentSize()
            setNeedsLayout()
        }
    }

    public override var intrinsicContentSize: CGSize {
        var height = min(.infinity, contentSize.height)
        let oneCellHeight = height / CGFloat(self.getItems().count)
        if (height > oneCellHeight * CGFloat(maxCell)) {
            height = oneCellHeight * CGFloat(maxCell)
        }else{
            self.isScrollEnabled = false
        }
        return CGSize(width: contentSize.width, height: height)
    }
}
