//
//  File 2.swift
//  
//
//  Created by Dmitry Bogomazov on 13.07.2021.
//

import Foundation
import UIKit

public class DBViewHelper {
    public class func initStackViewWithScrollView(view: UIView) -> (stackView: UIStackView, scrollView: UIScrollView) {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(stackView)
        
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0).isActive = true
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1).isActive = true
        
        return (stackView, scrollView)
    }
    public class func initStackViewWithScrollView(vc: UIViewController) -> (stackView: UIStackView, scrollView: UIScrollView) {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        vc.view.addSubview(scrollView)
        if #available(iOS 11.0, *) {
            scrollView.topAnchor.constraint(equalTo: vc.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        } else {
            scrollView.topAnchor.constraint(equalTo: vc.view.topAnchor, constant: 0).isActive = true
        }
        scrollView.trailingAnchor.constraint(equalTo: vc.view.trailingAnchor, constant: 0).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: vc.view.leadingAnchor, constant: 0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: vc.view.bottomAnchor, constant: 0).isActive = true
        
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(stackView)
        
        stackView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0).isActive = true
        stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 1).isActive = true
        
        return (stackView, scrollView)
    }
    public class func embdedToView(subView: UIView, leftConstraint: CGFloat? = nil, rightConstraint: CGFloat? = nil, topConstraint: CGFloat? = nil, bottomConstraint: CGFloat? = nil, heightConstraint: CGFloat? = nil, widthConstraint: CGFloat? = nil) -> UIView{
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        subView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subView)
        if topConstraint != nil {
            let s = subView.topAnchor.constraint(equalTo: view.topAnchor, constant: topConstraint!)
            s.isActive = true
        }
        
        if rightConstraint != nil {
            subView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: rightConstraint!).isActive = true
        }
        
        if leftConstraint != nil {
            subView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leftConstraint!).isActive = true
        }
        
        if bottomConstraint != nil {
            subView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottomConstraint!).isActive = true
        }
        
        
        if heightConstraint != nil {
            subView.heightAnchor.constraint(equalToConstant: heightConstraint!).isActive = true
        }
        
        if widthConstraint != nil {
            subView.widthAnchor.constraint(equalToConstant: widthConstraint!).isActive = true
        }
        
        return view
    }
    public class func getLabelForUserField(string: String?) -> UILabel{
            let label = UILabel()
            label.numberOfLines = 0
            label.text = string == nil ? "" : string!
            return label
        }
    public class func getCloseButton() -> UIButton{
        let button = UIButton()
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.widthAnchor.constraint(equalToConstant: 30).isActive = true
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.setImage(UIImage(named: "ic_db_close", in: Bundle.module, compatibleWith: nil), for: .normal)
        return button
    }
    
}
