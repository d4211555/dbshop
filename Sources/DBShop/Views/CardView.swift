//
//  CardView.swift
//  CosmedexiOSClient
//
//  Created by Dmitrii on 21/07/2019.
//  Copyright © 2019 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

open class DBCardView: UIView {
    
    var borderRadius : CGFloat = 2
    var stackView: UIStackView!
    
    public init(top: CGFloat = 4, borderRadius: CGFloat = 2) {
        super.init(frame: CGRect.zero)
        self.borderRadius = borderRadius
        setupViews(top: top)
    }
    
    public init(top: CGFloat = 4, right: CGFloat = 4, left: CGFloat = 4, bottom: CGFloat = 4, isCentered: Bool = false, borderRadius: CGFloat = 2) {
        super.init(frame: CGRect.zero)
        self.borderRadius = borderRadius
        setupViews(top: top, right: right, left: left, bottom: bottom, isCentered: isCentered)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews(top: CGFloat = 4, right: CGFloat = 4, left: CGFloat = 4, bottom: CGFloat = 4, isCentered: Bool = false){
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = borderRadius
        view.clipsToBounds = true
        self.addSubview(view)
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: top).isActive = true
        
        if (isCentered) {
            view.trailingAnchor.constraint(lessThanOrEqualTo: self.trailingAnchor, constant: -right).isActive = true
            view.leadingAnchor.constraint(greaterThanOrEqualTo: self.leadingAnchor, constant: left).isActive = true
        }else{
            view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -right).isActive = true
            view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: left).isActive = true
        }
        
        
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -bottom).isActive = true
        
        
        
        
        stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(stackView)
        
        //stackView.backgroundColor = UIColor.red
        stackView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
        stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    }
    
    private func addSeparator() {
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = DBColors.lightGrayColor
        self.stackView.addArrangedSubview(separatorView)
        separatorView.heightAnchor.constraint(equalToConstant: 1.0).isActive = true
    }
    
    private func attachToViewAndNotAdd(view: UIView, action: (() -> ())?, top: CGFloat = 16, bottom: CGFloat = 16, left: CGFloat = 16, right: CGFloat = 16) -> (parentView: UIView, childView: UIView){
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = true
        let parentView = UIView()
        if action != nil {
            parentView.addAction(action: action!)
        }
        parentView.addSubview(view)
        view.topAnchor.constraint(equalTo: parentView.topAnchor, constant: top).isActive = true
        view.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: bottom - bottom*2).isActive = true
        view.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: left).isActive = true
        view.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: right - right*2).isActive = true
        
        return (parentView, view)
    }
    
    private func attachToView(view: UIView, action: (() -> ())?, top: CGFloat = 16, bottom: CGFloat = 16, left: CGFloat = 16, right: CGFloat = 16, height: CGFloat? = nil) -> UIView {
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = true
        let parentView = UIView()
        if action != nil {
            parentView.addAction(action: action!)
        }
        parentView.addSubview(view)
        view.topAnchor.constraint(equalTo: parentView.topAnchor, constant: top).isActive = true
        view.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: bottom - bottom*2).isActive = true
        view.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: left).isActive = true
        view.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: right - right*2).isActive = true
        if (height != nil) {
            view.heightAnchor.constraint(equalToConstant: height!).isActive = true
        }
        self.stackView.addArrangedSubview(parentView)
        
        return view
    }
    
    //MARK: - add views
    
    public func createTextFieldWithLabel(delegate: UITextFieldDelegate, placeholder: String, string: String, top: CGFloat = 0, bottom: CGFloat = 0) -> UITextField {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.darkGray
        label.text = placeholder
        addView(view: label, top: top, bottom: 0, separator: false, action: nil)
        
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = placeholder
        textField.text = string
        textField.delegate = delegate
        addView(view: textField, top: 8, bottom: bottom, height: 50, separator: false, action: nil)
        return textField
    }
    
    public func addTitle(string: String, buttons: [UIButton] = [UIButton]()) {
        let returnViews = attachToViewAndNotAdd(view: UILabel(),action: nil, bottom: 8)
        let label = returnViews.childView as! UILabel
        label.textColor = DBColors.blueColor
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = string
        label.numberOfLines = 2
        let titleStackView = UIStackView()
        titleStackView.axis = .horizontal
        titleStackView.translatesAutoresizingMaskIntoConstraints = false
        titleStackView.addArrangedSubview(returnViews.parentView)
        
        for (index, button) in buttons.enumerated() {
            var right : CGFloat = 5.0
            var left : CGFloat = 5.0
            if index == 0 {
                left = 16.0
            }
            if index == buttons.count - 1 {
                right = 16.0
            }
            let view = attachToViewAndNotAdd(view: button, action: nil, bottom: 8, left: left, right: right)
            button.heightAnchor.constraint(equalToConstant: 20).isActive = true
            button.widthAnchor.constraint(equalToConstant: 20).isActive = true
            titleStackView.addArrangedSubview(view.parentView)
        }
        
        self.stackView.addArrangedSubview(titleStackView)
        
    }
    
    public func addCMButton(title: String, top: CGFloat = 12, bottom: CGFloat = 12, left: CGFloat = 16, right: CGFloat = 16, action: @escaping (() -> ())) {
        let changeStatusButton = DBButton()
        changeStatusButton.primary()
        changeStatusButton.setTitle(title, for: .normal)
        changeStatusButton.titleLabel?.numberOfLines = 0
        changeStatusButton.sizeToFit()
        changeStatusButton.addAction(action: action)
        self.addView(view: changeStatusButton, top: top, bottom: bottom, left: left, right: right, height: 50, action: nil)
    }
    
    public func addLabel(title: String, font: UIFont? = nil, separator: Bool = true) {
        let changeStatusButton = UILabel()
        changeStatusButton.text = title
        if (font != nil) {
            changeStatusButton.font = font
        }
        self.addView(view: changeStatusButton, separator: separator, action: nil)
    }
    
    public func addTextField(placeholder: String, separator: Bool = true) {
        let textField = UITextField()
        textField.placeholder = placeholder
        self.addView(view: textField, separator: separator, action: nil)
    }
    
    public func addTextView(separator: Bool = true) {
        let textView = UITextView()
        textView.layer.cornerRadius = 2
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = DBColors.blueColor.cgColor
        textView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        self.addView(view: textView, separator: separator, action: nil)
    }
    
    
    public func addView(view: UIView, top: CGFloat = 12, bottom: CGFloat = 12, left: CGFloat = 16, right: CGFloat = 16, height: CGFloat? = nil, separator: Bool = true, action: (() -> ())?) {
        if view is UILabel {
            (view as! UILabel).numberOfLines = 0
        }
        _ = attachToView(view: view, action: action, top: top, bottom: bottom, left: left, right: right, height: height)
        if (separator) {
            addSeparator()
        }
        
    }
    
    deinit {
        print("CARD VIEW DEINIT")
    }
}
