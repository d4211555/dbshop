//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 14.08.2021.
//

import Foundation
import UIKit
import DBTableCollectionView

public class DBButtonCellItem: DBModel {
    var title: String!
    var textColor: UIColor!
    var color: UIColor!
    var separator: Bool!
    public var tag: Int!
    
    public init(title: String, textColor: UIColor, color: UIColor, separator: Bool = true, tag: Int = 0) {
        self.title = title
        self.textColor = textColor
        self.color = color
        self.separator = separator
        self.tag = tag
    }
}

public protocol DBButtonTableViewCellDelegate: DBProtocol {
    func buttonDidTapped(item: DBButtonCellItem)
}

public class DBButtonTableViewCell: DBTableViewCell<DBButtonCellItem, DBButtonTableViewCellDelegate> {
    
    var button: UIButton!
    
    public class func getConfiguration(delegate: DBButtonTableViewCellDelegate?) -> DBTableViewCellConfig{
        return super.getConfiguration(delegate: delegate, nibName: nil, registerType: .registerClass)
    }
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(button)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.layoutAttachAll()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func configureCell(item: DBButtonCellItem) {
        if (item.separator) {
            self.separatorInset = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
        }else{
            self.separatorInset = UIEdgeInsets(top: 0, left: 1500, bottom: 0, right: 0)
        }
        button.setTitle(item.title, for: .normal)
        button.setTitleColor(item.textColor, for: .normal)
        button.backgroundColor = item.color
        button.addAction { [unowned self] in
            self.getDelegate()?.buttonDidTapped(item: item)
        }
    }
    
    deinit {
        print("DBButtonTableViewCell deinit")
    }
    
}
