//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 31.05.2021.
//

import Foundation
import UIKit

open class DBColors{
    
    static func infoForKey(_ key: String) -> String? {
            return (Bundle.main.infoDictionary?[key] as? String)?
                .replacingOccurrences(of: "\\", with: "")
     }
    
    public static var primaryColor = UIColor(named: "primary_color", in: .main, compatibleWith: nil)
    public static var secondaryColor = UIColor(named: "secondary_color", in: .main, compatibleWith: nil)
    
    public static var primaryButtonTextColor = UIColor(named: "primary_button_text_color", in: .main, compatibleWith: nil)
    public static var secondaryButtonTextColor = UIColor(named: "secondary_button_text_color", in: .main, compatibleWith: nil)
    
    
    public static var purpleColor: UIColor = UIColor(red: 88/255, green: 86/255, blue: 214/255, alpha: 1)//#5856D6
    
    public static var blueColor: UIColor = UIColor(red: 124/255, green: 175/255, blue: 255/255, alpha: 1)//#7CAFFF
    
    public static var grayColor: UIColor = UIColor(red: 184/255, green: 184/255, blue: 184/255, alpha: 1)

    public static var lightGrayColor: UIColor = UIColor(red: 199/255, green: 199/255, blue: 204/255, alpha: 1)
    
    public static var greenColor: UIColor = UIColor(red: 76/255, green: 175/255, blue: 80/255, alpha: 1)
    
    public static var redColor: UIColor = UIColor(red: 244/255, green: 67/255, blue: 54/255, alpha: 1)
    
    public static var backgroundGrayColor: UIColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
    
    public static var itemBorderColor: UIColor = UIColor(red: 199/255, green: 199/255, blue: 204/255, alpha: 1)
    
    public static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

public class DBVariables {
    public static var imageScale: CGFloat = 0.4
    public static var itemCornerRadius: CGFloat = 10
    public static var itemBorderWidth: CGFloat = 0.2
    public static var itemBorderColor: UIColor = DBColors.lightGrayColor
    public static var tableBackgroundColor = DBColors.backgroundGrayColor
}
