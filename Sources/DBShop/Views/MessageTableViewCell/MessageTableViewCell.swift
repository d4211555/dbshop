//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 09.08.2021.
//

import Foundation
import UIKit
import DBTableCollectionView

public protocol DBMessageTableViewCellDelegate: DBProtocol {
    func iconDidTapped(item: DBMessageItem)
}

public class DBMessageItem: DBModel {
    var title: String!
    var icon: UIImage?
    var backgroundColor: UIColor!
    var textColor: UIColor!
    var iconColor: UIColor?
    
    public init(title: String, icon: UIImage?, backgroundColor: UIColor, textColor: UIColor, iconColor: UIColor? = DBColors.secondaryColor) {
        self.title = title
        self.icon = icon
        self.backgroundColor = backgroundColor
        self.textColor = textColor
        self.iconColor = iconColor
    }
}

open class DBMessageTableViewCell: DBTableViewCell<DBMessageItem, DBMessageTableViewCellDelegate> {
    
    var iconImageLeadingConstraint: NSLayoutConstraint!
    var iconImageWidthConstraint: NSLayoutConstraint!
    var label: UILabel!
    var iconImage: UIImageView!
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        iconImage = UIImageView()
        self.contentView.addSubview(iconImage)
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        iconImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 16).isActive = true
        iconImageLeadingConstraint = iconImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 16)
        iconImageLeadingConstraint.isActive = true
        iconImageWidthConstraint = iconImage.widthAnchor.constraint(equalToConstant: 25)
        iconImageWidthConstraint.isActive = true
        iconImage.heightAnchor.constraint(equalToConstant: 25).isActive = true
        iconImage.isUserInteractionEnabled = true
        iconImage.addAction { [unowned self] in
            self.getDelegate()?.iconDidTapped(item: self.item)
        }
        
        label = UILabel()
        label.numberOfLines = 0
        self.contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 16).isActive = true
        label.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -16).isActive = true
        label.leadingAnchor.constraint(equalTo: iconImage.trailingAnchor, constant: 8).isActive = true
        label.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -16).isActive = true
        label.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
    
    }
    
    required public init?(coder: NSCoder) {
        super.init(coder: coder)
        
       
    }
    open override func configureCell(item: DBMessageItem) {
        if (item.icon == nil) {
            iconImageLeadingConstraint.constant = 0
            iconImageWidthConstraint.constant = 0
        }else{
            iconImageLeadingConstraint.constant = 16
            iconImageWidthConstraint.constant = 25
        }
        iconImage.image = item.icon
        
        
        if (item.iconColor != nil) {
            iconImage.tintColor = item.iconColor
        }else{
            iconImage.tintColor = item.textColor
        }
        label.text = item.title
        label.textColor = item.textColor
        self.contentView.backgroundColor = item.backgroundColor
    }
    
    public class func getConfiguration(delegate: DBMessageTableViewCellDelegate?) -> DBTableViewCellConfig{
        return super.getConfiguration(delegate: delegate, nibName: nil, registerType: .registerClass)
    }
}
