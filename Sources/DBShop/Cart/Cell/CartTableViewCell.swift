                                                                                                             


import UIKit
import SDWebImage
import DBTableCollectionView

@objc public protocol DBCartTableViewCellDelegate: DBProtocol {
    func countChanged(_ item : DBCartItemModel)
    func cellDidSelect(item: DBCartItemModel)
    
    //func moreButtonTapped(item: DBCartItemModel)
}


open class DBCartTableViewCell<M: DBCartItemModel, D: DBCartTableViewCellDelegate>: DBTableViewCell<M, D>{

    @IBOutlet public weak var minusButton: DBButton!
    @IBOutlet public weak var totalPrice: UILabel?
    @IBOutlet public weak var countLabel: UILabel!
    @IBOutlet public weak var plusButton: DBButton!
    @IBOutlet public weak var productImage: UIImageView!
    @IBOutlet public weak var nameLabel: UILabel!
    @IBOutlet public weak var priceLabel: UILabel!
    @IBOutlet public weak var previousPriceLabel: UILabel?
   // var item: BucketOrderModel!
    
    @IBAction open func plusButtonAction(_ sender: UIButton) {
        self.item.count = self.item.count + 1
        self.countLabel!.text = "\(self.item.count!)"
        self.newCount()
        getDelegate()?.countChanged(self.item)
    }
    
    open func deleteItemOnOneCount(item: M) {
        
    }
    
    @IBAction open func minusButtonAction(_ sender: UIButton) {
        if self.item.count-1 >= 1{
            self.countLabel!.text = "\(self.item.count-1)"
            self.item.count = self.item.count - 1
            self.newCount()
            getDelegate()?.countChanged(self.item)
        }else{
            self.deleteItemOnOneCount(item:self.item)
        }
    }
   
    open func newCount() {
        if let price = self.item.product.getProductTotalPrice(count: self.item.count) {
            priceLabel.text = DBStringUtil.convertStringValue(i: price)
        }
        if let prevPrice = self.item.product.getProductTotalPreviousPrice(count: self.item.count) {
            previousPriceLabel?.isHidden = false
            previousPriceLabel?.attributedText = DBStringUtil.makeStrikePrice(price: prevPrice)
        }else{
            previousPriceLabel?.isHidden = true
            previousPriceLabel?.attributedText = NSAttributedString()
        }
    }
    
}
