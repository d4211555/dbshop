//
//  BucketPresenter.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 06.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import RxSwift

public protocol DBCartListLoadingView: DBLoadingView {
    associatedtype T: DBCartItemModel
    func productDeleted(cartModel: T)
    //func allCartLoaded(cartModels: [T])
    func allProductsDeleted()
}

open class DBCartLoadingPresenter<Product: DBProduct, Model: DBCartItemModel, Delegate: DBCartListLoadingView, CoreDataCart: DBCoreDataCartProtocol>: DBLoadingPresenter<Delegate>{
    
    var coreDataManager: CoreDataCart? = nil
    
    open override func start() {
        super.start()
        DBAppLocalNotification.addLocalSubscriber(key: getClassName(), localEventType: [.updateCart]) { (localEvent) in
            self.getProducts()
        }
        DBAppLocalNotification.localDisposableMap.forEach { (key: String, value: Disposable) in
            print("key = \(key) & vale = \(value)")
        }
    }
    
    public init(view: Delegate?, coreDataManager: CoreDataCart?, coreDataAuthManager: DBCoreDataAuthProtocol?) {
        super.init(view: view, coreDataAuthManager: coreDataAuthManager)
        self.coreDataManager = coreDataManager
    }
    
    open func getProductsDataSource(ids: [Int]) -> Single<[Product]> {
        fatalError("need to override")
    }
    
    open func allCartLoaded(cartModels: [Model]) {
        fatalError("need to override")
    }
    
    public func getProducts() {
        guard let cartModels = coreDataManager?.getAllItems() else {
            self.view?.showEmptyItem()
            return
        }
        if (cartModels.isEmpty) {
            self.view?.showEmptyItem()
            return
        }
        
        view?.showLoading()
        let ids = cartModels.map { (cart) -> Int in
            return (cart as DBCartItemModel).productId
        }
        getProductsDataSource(ids: ids)
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { [self] (products) in
                var deleteCartArray = [DBCartItemModel]()
                var cartArray = [DBCartItemModel]()
                cartModels.forEach { (cart) in
                    let p = products.first(where: { (product) -> Bool in
                        let s = Mirror(reflecting: product).children.first{$0.label == "id"}?.value
                        //let price = Mirror(reflecting: product).children.first{$0.label == "price"}?.value as? Int
                        return ((s as! Int) == cart.productId) && (product.ifPriceAvailable())
                    })
                    if (p == nil) {
                        deleteCartArray.append(cart)
                    }else{
                        cart.product = p
                        cartArray.append(cart)
                    }
                }
                deleteCartArray.forEach { (model) in
                    let _ = coreDataManager?.deleteItem(productId: model.productId)
                    DBRuntimeStorageSingleton.sharedInstance.removeCartItem(productId: model.productId)
                }
                DBAppLocalNotification.sendLocalNotification(notificationType: .updateOrderBadge)
                if (cartArray.isEmpty) {
                    self.view?.showContent()
                    self.view?.showEmptyItem()
                    return
                }
                self.allCartLoaded(cartModels: cartArray as! [Model])
                //self.view?.showContent()
            }, onFailure: onAppRetryError)
            .disposed(by: disposeBag)
    }
    
    open func removeProduct(cartModel: Model) {
        if ((coreDataManager?.deleteItem(productId: cartModel.productId)) != nil) {
            self.view?.productDeleted(cartModel: cartModel as! Delegate.T)
            DBRuntimeStorageSingleton.sharedInstance.removeCartItem(productId: cartModel.productId)
            DBAppLocalNotification.sendLocalNotification(notificationType: .updateOrderBadge)
            DBAppLocalNotification.sendLocalNotification(notificationType: .productRemovedFromCart, objectId: cartModel.productId)
        }
    }
    
    open func newCountForProduct(cartItem: Model, count : Int) {
        DBRuntimeStorageSingleton.sharedInstance.addCartItem(productId: cartItem.productId, count: count)
        coreDataManager?.changeCountForItem(productId: cartItem.productId, count: count)
        DBAppLocalNotification.sendLocalNotification(notificationType: .updateOrderBadge)
    }
    
    open func removeAllProducts() {
        
        let a = coreDataManager?.getAllItems()
        if a == nil {
            return
        }
        coreDataManager?.deleteAllItems()
        
        a!.forEach { cartItem in
            DBAppLocalNotification.sendLocalNotification(notificationType: .productRemovedFromCart, objectId: cartItem.productId)
        }
        DBAppLocalNotification.sendLocalNotification(notificationType: .updateCart)
        DBRuntimeStorageSingleton.sharedInstance.removeAllCartItems()
        DBAppLocalNotification.sendLocalNotification(notificationType: .updateOrderBadge)
        self.view?.allProductsDeleted()
    }
}
