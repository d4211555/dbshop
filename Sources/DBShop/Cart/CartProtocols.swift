//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 28.05.2021.
//

import Foundation

public protocol DBCoreDataCartProtocol {
    associatedtype T: DBCartItemModel
    func addItem(productId: Int, count: Int) -> Bool
    func addItem(item: T) -> Bool
    func getItem(productId: Int) -> T?
    func deleteItem(productId: Int) -> Bool
    func deleteAllItems()
    func getAllItems() -> [T]
    func changeCountForItem(productId: Int, count: Int)
}

public protocol DBCoreDataFavouriteProtocol {
    associatedtype B: DBFavouriteItemModel
    func addFavouriteItem(productId: Int) -> Bool
    func getFavouriteItem(productId: Int) -> B?
    func deleteFavouriteItem(productId: Int) -> Bool
    func deleteAllFavouriteItems() -> Bool
    func getAllFavouriteItems() -> [B]
}

public protocol DBCoreDataAuthProtocol {
    func setToken(_ token: String)
    func getToken() -> String?
    func clearToken()
    func logoutUser()
}
