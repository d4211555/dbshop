//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 16.06.2021.
//

import Foundation
//public protocol DBCartLoadingPresenterDelegate: DBLoadingView {
//    associatedtype T: DBCartItemModel
//    func allCartLoaded(cartModels: [T])
//    func productDeleted(cartModel: T)
//    func allProductsDeleted()
//    func emptyCartItems()
//}
//import UIKit
//import DBTableCollectionView
//import DBShop
//
//class DBCartViewController: BaseLoadingViewController, BucketTableViewCellDelegate, CreateOrderDelegate, CartPresenterDelegate {
//
//    @IBOutlet var tableView: DBTableView!
//    @IBOutlet var orderButton: UIButton!
//
//    @IBOutlet var totalPrice: EFCountingLabel!
//    @IBOutlet var createOrderView: UIView!
//    //var array = [BucketOrderModel]()
//    var presenter: BucketControllerPresenter<BucketViewController>? = nil
//    var section = DBTableViewSection(items: [DBModel]())
//    @IBOutlet var orderViewHeight: NSLayoutConstraint!
//    var createButton = false
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        self.totalPrice.animationDuration = 0.35
//        self.presenter = BucketControllerPresenter(view: self, coreDataManager: CoreDataEngine.sharedInstance)
//        presenter?.start()
//
//        tableView.setEmptyModel(name: "Ваша корзина пока пуста", image: UIImage(named: "emptyBucket"), height: tableView.frame.height)
//        tableView.registerCell(cell: EmptyCartCell.getConfiguration(delegate: nil))
//        tableView.registerCell(cell: BucketTableViewCell.getConfiguration(delegate: self))
//
//        self.totalPrice.formatBlock = {
//            (value) in
//            return StringUtil.convertStringValue(i: Int(value))
//        }
//        self.tableView.add(section: section)
//        self.initRefreshControl(tv: self.tableView)
//        reload()
//    }
//
//    override func refresh(sender: UIRefreshControl) {
//        super.refresh(sender: sender)
//        reload()
//    }
//
//    func emptyCartItems() {
//        self.tableView.showEmptyItem()
//        self.hideCreateOrderView()
//    }
//
//    func reload() {
//        presenter?.getProducts()
//    }
//
//    func allCartLoaded(cartModels: [CartItem]) {
//        if (cartModels.isEmpty){
//            self.emptyCartItems()
//            return
//        }
//        self.showCreateOrderView()
//        self.tableView.replaceAll(items: cartModels, in: section)
//        self.reloadTotalPrice()
//    }
//
//    func showCreateOrderView() {
//        self.orderViewHeight.constant = 50
//    }
//
//    func hideCreateOrderView() {
//        self.orderViewHeight.constant = 0
//    }
//
//    func countChanged(_ item : DBCartItemModel){
//        reloadTotalPrice()
//        presenter?.newCountForProduct(cartItem: (item as! CartItem), count: item.count)
//    }
//
//    func reloadTotalPrice(){
//        let array = self.tableView.getItems() as! [CartItem]
//        self.totalPrice.countFromCurrentValueTo(CGFloat(IntegerUtil.getTotalPrice(array: array)))
//    }
//
//    func cellDidSelect(item: DBCartItemModel) {
//        Router.toProductDetailViewController(vc: self, productId: (item as! CartItem).getProduct().id)
//    }
//
//    func productDeleted(cartModel: CartItem) {
//        self.tableView.remove(item: cartModel)
//        if (self.tableView.getItems().isEmpty){
//            self.emptyCartItems()
//        }
//        self.reloadTotalPrice()
//    }
//
//    func deleteCellTapped(item: CartItem) {
//        self.presenter?.removeProduct(cartModel: item)
//    }
//
//    @IBAction func createOrderButtonAction(_ sender: UIButton) {
//        Router.toCreateNewOrderViewController(vc: self, delegate: self, array: self.tableView.getItems() as! [CartItem])
//    }
//
//    func allProductsDeleted() {
//        self.tableView.showEmptyItem()
//        self.hideCreateOrderView()
//        self.totalPrice.countFromCurrentValueTo(0)
//    }
//
//    func clearBucket(){
//        presenter?.removeAllProducts()
//    }
//
//    deinit {
//        presenter?.stop()
//    }
//}
