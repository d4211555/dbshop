//
//  File 2.swift
//  
//
//  Created by Dmitry Bogomazov on 28.05.2021.
//

import Foundation
import DBTableCollectionView

open class DBProduct: DBModel {
    public var isInCart: Bool = false
    public var isInFavourite: Bool = false
    
    open func getProductTotalPrice(count: Int) -> Double?{
        fatalError("need override")
    }
    
    open func getProductTotalPreviousPrice(count: Int) -> Double?{
        fatalError("need override")
    }
    
    open func getCurrentPrice(count: Int) -> Double? {
        fatalError("need override")
    }
    
    open func ifPriceAvailable() -> Bool {
        fatalError("need override, checking if price of product is available")
    }
    
    public func getProductIdValue() -> Int{
        return Mirror(reflecting: self).children.first{$0.label == "id"}?.value as! Int
    }
    
    
    
}

public enum DBProductAvailableType: String, Codable {
    case available = "AVAILABLE"
    case notAvailable = "NOT_AVAILABLE"
    case underTheOrder = "UNDER_THE_ORDER"
    
    func getLocalized() -> String {
        switch self {
        case .available:
            return "В наличии"
        case .notAvailable:
            return "Нет в наличии"
        case .underTheOrder:
            return "Под заказ"
        }
    }
}

