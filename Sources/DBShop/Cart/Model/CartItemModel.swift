//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 28.05.2021.
//

import Foundation
import DBTableCollectionView

open class DBCartItemModel: DBModel {
    public var id: Int!
    public var count: Int!
    public var productId: Int!
    public var product: DBProduct!
    public var attributeId: Int!
}

open class DBFavouriteItemModel: DBModel {
    public var productId: Int!
    public var product: DBProduct!
}
