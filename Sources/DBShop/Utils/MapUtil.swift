//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 02.11.2021.
//

import Foundation
import MapKit

public class MapUtil {
    public class func openMap(_ lat: String?, _ long: String?, _ name: String?) {
        if (lat == nil || long == nil) {
            return
        }
        
        let latitude: CLLocationDegrees? = Double(lat!) ?? nil
        let longitude: CLLocationDegrees? = Double(long!) ?? nil
       
        if (latitude == nil || longitude == nil) {
            return
        }
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude!, longitude!)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = name
        mapItem.openInMaps(launchOptions: options)
    }
}
