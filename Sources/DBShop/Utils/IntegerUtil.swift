//
//  IntegerUtil.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 03.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation

public class IntegerUtil {
    
//    public class func getTotalPrice(array: [DBCartItemModel]) -> Int{
//        var totalPrice = 0
//        array.forEach { (order) in
//            let tPrice = order.product.getProductTotalPrice(count: order.count)
//            if tPrice != nil {
//                totalPrice = totalPrice + tPrice!
//            }
//        }
//        return totalPrice
//    }
    
    public class func roundOneDigit(value: Float) -> String{
        return String(format: "%.1f", value)
    }
    
    public class func decimalValue(value: Double) -> String{
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 1
        
        return formatter.string(from: NSNumber(value: value))!
    }
}
