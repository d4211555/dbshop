//
//  DateUtil.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 03.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
public class DBDateUtil {
    
    public static var fullDate = "dd MMMM yyyy HH:mm"
    public static var fullDateDotWithSecond = "dd.MM.YYYY HH:mm:ss"
    public static var dateWithWeekDay = "d MMMM, EEEE"
    public static var dateWithDay = "d MMMM"
    
    public static var dateTimeWithIn = "d MMMM в HH:mm" 
    
    public class func dateStringForView(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fullDate
        
        //dateFormatter.locale = Locale.current
        return dateFormatter.string(from:date)
    }
    
    public class func getDate(interval: TimeInterval, formatter: String) -> String{
        let date = Date(timeIntervalSince1970: interval / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatter
        return dateFormatter.string(from: date)
    }
    
    
    public class func getDateString(interval: TimeInterval) -> String{
        let newInterval = interval / 1000
        let date = Date(timeIntervalSince1970: newInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = fullDateDotWithSecond
        return dateFormatter.string(from: date)
    }
}
