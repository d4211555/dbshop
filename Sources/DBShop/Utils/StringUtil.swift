//
//  StringUtil.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 03.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

public class DBStringUtil {
    public class func getWelcomeString() -> String {
        var welcomeString = ""
        let hour = Calendar.current.component(.hour, from: Date())
        if hour >= 6 && hour < 12 {
            welcomeString = "доброе утро!"
        }else if hour >= 12 && hour < 18{
            welcomeString = "добрый день!"
        } else if hour >= 18 && hour < 22 {
            welcomeString = "добрый вечер!"
        }else{
            welcomeString = "доброй ночи!"
        }
        return welcomeString
    }
    public class func convertStringValue(str : String) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        return (formatter.string(from: NSNumber(value: Int(str)!)) ?? "") + " \u{20BD}"
    }
    
    public class func convertStringValue(i : Int) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        return (formatter.string(from: NSNumber(value: i)) ?? "") + " \u{20BD}"
    }
    
//    public class func convertStringValue(i : Double) -> String{
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .decimal
//        formatter.groupingSeparator = " "
//        return (formatter.string(from: NSNumber(value: i)) ?? "") + " \u{20BD}"
//    }
    
    public class func convertStringValue(i : Double, isAddCuurency: Bool = true) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        var s = (formatter.string(from: NSNumber(value: i)) ?? "")
        if (isAddCuurency) {
            s = s + " \u{20BD}"
        }
        return  s
    }
    
    public class func convertStringValue(i : NSNumber) -> String{
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = " "
        return (formatter.string(from: i) ?? "") + " \u{20BD}"
    }
    
    
    public class func makeStrikePrice(price: Int) -> NSAttributedString{
        let attributed = NSMutableAttributedString(string: convertStringValue(i: price))
        
        attributed.addAttributes([NSAttributedString.Key.strikethroughStyle: NSNumber(value: NSUnderlineStyle.single.rawValue as Int),
                                  NSAttributedString.Key.strikethroughColor: UIColor.red], range: NSMakeRange(0, attributed.string.count))
        return attributed
    }
    public class func makeStrikePrice(price: Double) -> NSAttributedString{
        let attributed = NSMutableAttributedString(string: convertStringValue(i: price))
        
        attributed.addAttributes([NSAttributedString.Key.strikethroughStyle: NSNumber(value: NSUnderlineStyle.single.rawValue as Int),
                                  NSAttributedString.Key.strikethroughColor: UIColor.red], range: NSMakeRange(0, attributed.string.count))
        return attributed
    }
    public class func makeStrikePrice(price: NSNumber) -> NSAttributedString{
        let attributed = NSMutableAttributedString(string: convertStringValue(i: price))
        
        attributed.addAttributes([NSAttributedString.Key.strikethroughStyle: NSNumber(value: NSUnderlineStyle.single.rawValue as Int),
                                  NSAttributedString.Key.strikethroughColor: UIColor.red], range: NSMakeRange(0, attributed.string.count))
        return attributed
    }
}
