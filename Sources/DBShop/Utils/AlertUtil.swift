//
//  AlertUtil.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 09.06.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

public class DBAlertUtil {
    public class func alertWithSuccess(vc: UIViewController, title: String?, message: String?, successAction: (() -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action) in
            successAction?()
        }))
        vc.present(alert, animated: true, completion: nil)
    }
    
    public class func alertShareProduct(vc: UIViewController, copyLink: @escaping () -> (), openBrowser open: @escaping () -> ()) {
        let alert = UIAlertController(title: "Поделиться продуктом", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Скопировать ссылку", style: .default, handler: { (action) in
            copyLink()
        }))
        alert.addAction(UIAlertAction(title: "Открыть в браузере", style: .default, handler: { (action) in
            open()
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (action) in
            
        }))
        vc.present(alert, animated: true, completion: nil)
    }
    
//    public class func showChooseAlert(vc: UIViewController, title: String?, message: String?, onSuccess: @escaping ()->()){
//            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Да", style: .default, handler: { (action) in
//                onSuccess()
//            }))
//            alert.addAction(UIAlertAction(title: "Нет", style: .destructive, handler: { (action) in
//            }))
//            vc.present(alert, animated: true, completion: nil)
//        }
}
