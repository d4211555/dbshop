//
//  File.swift
//  
//
//  Created by Dmitrii on 15.05.2023.
//

import Foundation

public class DBOrderUtil {
    public class func getTotalPrice(array: [DBCartItemModel]) -> Double{
        var totalPrice: Double = 0.0
        array.forEach { (order) in
            let tPrice = order.product.getProductTotalPrice(count: order.count)
            if tPrice != nil {
                totalPrice = totalPrice + tPrice!
            }
        }
        return totalPrice
    }
}
