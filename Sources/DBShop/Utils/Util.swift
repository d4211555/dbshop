//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 14.04.2022.
//

import Foundation
import UIKit

public class DBUtil {
    public class func callNumber(phone: String) {
            if let url = URL(string: "tel://+\(phone)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
}
