//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 24.01.2023.
//

import Foundation
import UIKit
import DBTableCollectionView

public class DBOrderShippingMethodItem: DBModel, Codable {
    public var minPrice: Int!
    public var name: String!
    public var desc: String!
    public var fields: [DBOrderShippingMethodField]!
    public var availableDates: [TimeInterval]?
    public var availableTimes: [String]?
    
    public func getAvailableDates() -> [String]{
        if (availableDates == nil) {
            return [String]()
        }
        return availableDates!.map{DBDateUtil.getDate(interval: $0, formatter: DBDateUtil.dateWithWeekDay)}
    }
    
   
    
    private enum CodingKeys: String, CodingKey {
        case minPrice, desc, name, fields, availableDates,availableTimes
      }
    
    public var isChose: Bool = false
    public var currentPrice: Double!
}

public enum DBOrderShippingMethodField: String, Codable {
    case time = "TIME"
    case date = "DATE"
}

public protocol DBOrderShippingMethodTableViewCellDegelate: DBProtocol {
    func cellDidSelect(item: DBOrderShippingMethodItem)
}

public class DBOrderShippingMethodTableViewCell: DBTableViewCell<DBOrderShippingMethodItem, DBOrderShippingMethodTableViewCellDegelate> {
     
    var nameLabel: UILabel!
    var disableDescriptionLabel: UILabel!
    var chooseButton: DBButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        removeHeaderSeparator()
        nameLabel = UILabel(frame: CGRect.zero)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(nameLabel)
        nameLabel.layoutAttachTop(margin: 8)
        nameLabel.layoutAttachLeading(margin: 16)
        
        disableDescriptionLabel = UILabel(frame: CGRect.zero)
        disableDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        disableDescriptionLabel.font = UIFont.systemFont(ofSize: 10)
        disableDescriptionLabel.textColor = UIColor.lightGray
        
        self.contentView.addSubview(disableDescriptionLabel)
        disableDescriptionLabel.layoutAttachTop(to: nameLabel, margin: 4)
        disableDescriptionLabel.layoutAttachLeading(margin: 16)
        disableDescriptionLabel.layoutAttachBottom(margin: 8)
        
        chooseButton = DBButton()
        chooseButton.primary()
        chooseButton.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(chooseButton)
        //chooseButton.layoutAttachTop(margin: 8, relatedBy: .greaterThanOrEqual)
        //chooseButton.layoutAttachBottom(margin: 8, relatedBy: .greaterThanOrEqual)
        chooseButton.layoutAttachTrailing(margin: 16)
        chooseButton.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        chooseButton.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        chooseButton.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 0).isActive = true
        //chooseButton.titleLabel?.adjustsFontSizeToFitWidth = true
        chooseButton.titleLabel!.baselineAdjustment = .alignCenters
        //chooseButton.layoutAttachWidth(constant: 60, priority: UILayoutPriority(999))
        chooseButton.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        chooseButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        
        disableDescriptionLabel.layoutAttachTrailing(to: chooseButton, margin: 16)
        nameLabel.layoutAttachTrailing(to: chooseButton, margin: 16)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func cellDidSelect(item: DBOrderShippingMethodItem) {
        getDelegate()?.cellDidSelect(item: item)
    }
    
    public class func getConfiguration(delegate: DBOrderShippingMethodTableViewCellDegelate?) -> DBTableViewCellConfig{
        
        return DBTableViewCellConfig(type: DBOrderShippingMethodTableViewCell.self, cellIdentifier: "DBOrderShippingMethodTableViewCell", delegate: delegate, modelType: DBOrderShippingMethodItem.self, registerType: .registerClass, nibName: nil)
    }
    
    public override func configureCell(item: DBOrderShippingMethodItem) {
        self.nameLabel.text = item.desc
        self.setChooseButton()
        if (Double(item.minPrice) > item.currentPrice) {
            self.showDisableDescriptionLabel()
            self.disableDescriptionLabel.text = "Доступно при заказе от \(item.minPrice!) рублей"
        }else{
            self.hideDisableDescriptionLabel()
        }
        
        chooseButton.addAction { [unowned self] in
            self.item.isChose = true
            self.cellDidSelect(item: item)
        }
        
    }
    
    func showDisableDescriptionLabel() {
        self.disableDescriptionLabel.isHidden = false
        self.chooseButton.setTitle("Выбрать", for: .normal)
        self.chooseButton.setNotEnabled()
        self.nameLabel.textColor = UIColor.lightGray
    }
    
    func hideDisableDescriptionLabel() {
        self.disableDescriptionLabel.text = ""
        self.disableDescriptionLabel.isHidden = true
        self.chooseButton.setPrimaryEnabled()
        self.nameLabel.textColor = UIColor.black
        setChooseButton()
    }
    
    func setChooseButton() {
        if (self.item.isChose) {
            self.chooseButton.setTitle("Выбрано", for: .normal)
            self.chooseButton.greenBG()
        }else{
            self.chooseButton.setTitle("Выбрать", for: .normal)
            self.chooseButton.primary()
        }
        
    }
}
