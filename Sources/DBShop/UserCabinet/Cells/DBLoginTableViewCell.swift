//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 14.07.2021.
//

import Foundation
import UIKit
import DBTableCollectionView

public protocol DBLoginTableViewCellDelegate: DBProtocol {
    func loginButtonTapped()
}

public class DBLoginTableViewCellItem: DBModel {
    var title: String!
    var text: String!
    
    public init(title: String, text: String) {
        self.title = title
        self.text = text
    }
}

public class DBLoginTableViewCell: DBTableViewCell<DBLoginTableViewCellItem, DBLoginTableViewCellDelegate> {
    
    @IBOutlet weak var loginButton: DBButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var txtLabel: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.loginButton.primary()
        self.selectionStyle = .none
    }
    
    public class func getConfiguration(delegate: DBLoginTableViewCellDelegate?) -> DBTableViewCellConfig{
        return super.getConfiguration(delegate: delegate, nibName: "DBLoginTableViewCell", bundle: Bundle.module, registerType: .registerNib)
    }
    
    public override func configureCell(item: DBLoginTableViewCellItem) {
        self.titleLabel.text = item.title
        self.txtLabel.text = item.text
    }
    
    @IBAction func loginButtonTapped(_ sender: DBButton) {
        getDelegate()?.loginButtonTapped()
    }
    
}
