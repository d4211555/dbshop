//
//  ProductGalleryHorizontalCell.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 12.05.2021.
//  Copyright © 2021 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import DBTableCollectionView
import UIKit
import SDWebImage
// UIScreen.main.bounds.width / 414 *


public class DBShortOrderSection: DBCollectionViewSection {
    
    public override func getConfiguration(isHeader: Bool = false) {
        let height = UIScreen.main.bounds.width * 0.65 / 375
        self.setConfigutarion(lineSpacing: 0, insets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), widthRatio: 0.95, heightRatio: height, itemCount: 2, scrollBehavior: .continuous)
    }
}

public class DBLegacyShortOrderSection: DBCollectionViewSection {
    
    public override func getConfiguration(isHeader: Bool = false) {
        print("iphone screen  = \(UIScreen.main.bounds.width)" )
        
        let height = 375 * 0.65 / UIScreen.main.bounds.width
        print("height  = \(height)" )
        self.setConfigutarion(lineSpacing: 10, insets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20), widthRatio: 1, heightRatio: height, itemCount: 2, scrollBehavior: .continuous, horizontalTableView: true)
    }
}



public class DBShortOrderHorizontalCell: DBHorizontalTableViewCell<DBHorizontalItem, DBShortOrderTableViewCellDelegate> {
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.removeHeaderSeparator()
    }
}

public protocol DBShortOrderTableViewCellDelegate: DBProtocol {
    func shortOrderDidSelect(item: DBShortOrderItem)
}

public class DBShortOrderCollectionViewCell: DBCollectionViewCell<DBShortOrderItem, DBShortOrderTableViewCellDelegate> {
    
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var deliveryMethodLabel: UILabel!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.layer.cornerRadius = 10
        self.contentView.clipsToBounds = true
    }
    
    public override func configureCell(item: DBShortOrderItem) {
        self.orderNumberLabel.text = "№\(String(describing: item.orderNumber!))"
        self.priceLabel.text = DBStringUtil.convertStringValue(i: item.price)
        if (item.shippingMethod == nil) {
            self.deliveryMethodLabel.text = item.deliveryMethod
        }else{
            self.deliveryMethodLabel.text = item.shippingMethod?.desc
        }
        
    }
    
    public override func cellDidSelect(item: DBShortOrderItem) {
        getDelegate()?.shortOrderDidSelect(item: item)
    }
    
}

public class DBShortOrderItem: DBModel {
    public var orderId: Int!
    var orderNumber: String!
    var price: Int!
    var deliveryMethod: String!
    var shippingMethod: DBOrderShippingMethodItem?
    var date: String?
    
    public init(orderId: Int, orderNumber: String, price: Int, deliveryMethod: String, shippingMethod: DBOrderShippingMethodItem?, date: String?) {
        self.orderId = orderId
        self.orderNumber = orderNumber
        self.price = price
        self.deliveryMethod = deliveryMethod
        self.shippingMethod = shippingMethod
        self.date = date
    }
}
