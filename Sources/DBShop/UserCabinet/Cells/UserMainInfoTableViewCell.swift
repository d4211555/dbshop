//
//  UserMainInfoTableViewCell.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 14.07.2021.
//  Copyright © 2021 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import DBTableCollectionView
import UIKit
import SDWebImage

public protocol DBUserMainInfoTableViewCellDelegate: DBProtocol {
    func logoutButtonTapped()
    func imageTapped()
}

public class DBUserMainInfoItem: DBModel {
    var firstName: String!
    var lastName: String!
    var userAvatar: URL!
    var someDescription: String!
    
    public init(firstName: String, lastName: String, userAvatar: URL, someDescription: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.userAvatar = userAvatar
        self.someDescription = someDescription
    }
    
}

public class DBUserMainInfoTableViewCell: DBTableViewCell<DBUserMainInfoItem, DBUserMainInfoTableViewCellDelegate> {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userAccessLabel: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    
    public class func getConfiguration(delegate: DBUserMainInfoTableViewCellDelegate?) -> DBTableViewCellConfig{
        return super.getConfiguration(delegate: delegate, nibName: "DBUserMainInfoTableViewCell", bundle: Bundle.module, registerType: .registerNib)
    }
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.userImageView.layer.cornerRadius = self.userImageView.frame.height / 2
        self.userImageView.layer.masksToBounds = true
        self.userImageView.isUserInteractionEnabled = true
        self.selectionStyle = .none
        self.userImageView.addAction { [unowned self] in
            self.getDelegate()?.imageTapped()
        }
    }
    
    public override func configureCell(item: DBUserMainInfoItem) {
        self.userNameLabel.text = item.firstName + " " + item.lastName
        self.userImageView.sd_setImage(with: item.userAvatar, placeholderImage: DBImage.avatarPlaceholder(), options : [.retryFailed, .refreshCached])
        //self.userImageView.updateCache()
        
//        self.userImageView.sd_setImage(with:  item.userAvatar) { (image, error, cache, urls) in
//                    if (image != nil) {
//                        self.userImageView.image = image
//                    } else {
//                        self.userImageView.image = DBImage.avatarPlaceholder()
//                    }
//        }
        self.userAccessLabel.text = item.someDescription
    }
    
    @IBAction func logoutButtonTapped(_ sender: UIButton) {
        getDelegate()?.logoutButtonTapped()
    }
    
}
