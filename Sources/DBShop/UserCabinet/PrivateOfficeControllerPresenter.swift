//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 22.07.2021.
//

import Foundation
import RxSwift

public protocol DBPrivateOfficeControllerPresenterDelegate: DBLoadingView {
    func setEventsCount(count: Int)
}

open class DBPrivateOfficeControllerPresenter<T: DBPrivateOfficeControllerPresenterDelegate, Event: DBRuntimeEvent>: DBLoadingPresenter<T>{

    open func load() {
        
    }
    
    override open func start() {
        super.start()
        DBAppLocalNotification.addLocalSubscriber(key: getClassName()+"_updateEvents", localEventType: [.updateEventsCount, .updateUserCabinet]) { (localEvent) in
            if (localEvent.type == DBChangesNotificationType.updateEventsCount) {
                self.view?.setEventsCount(count: DBRuntimeStorageSingleton.sharedInstance.getRuntimeEventsCount())
            }
            
            if (localEvent.type == DBChangesNotificationType.updateUserCabinet) {
                self.load()
            }
        }
    }

//    open func getEventCountDataSource() -> Single<[Event]>{
//        fatalError("need to override to get favourite count")
//    }
////
//    func loadEventItems() {
//        getEventCountDataSource()
//            .observe(on: MainScheduler.instance)
//            .subscribe(onSuccess: { (events) in
//                DBRuntimeStorageSingleton.sharedInstance.setRuntimeEvents(runtimeEvents: events)
//                self.view?.setEventsCount(count: events.count)
//            }, onFailure: onAppRetryError)
//            .disposed(by: disposeBag)
//
//    }
    
    public func loadEventItems(events: [Event]) {
        DBRuntimeStorageSingleton.sharedInstance.setRuntimeEvents(runtimeEvents: events)
        self.view?.setEventsCount(count: events.count)
        
    }
    
    open override func stop() {
        super.stop()
        DBAppLocalNotification.removeLocalSubscriber(key: getClassName()+"_updateEvents")
    }
}
