//
//  BasePresenter.swift
//  youradvisor
//
//  Created by Дмитрий on 14.02.2020.
//  Copyright © 2020 iCApps. All rights reserved.
//

import Foundation
import RxSwift
import FirebaseInstanceID

protocol DBBasePresenter {
    func start()
    func stop()
    
}

@objc public protocol DBLoadingView: AnyObject {
    func showLoading()
    func showContent()
    func showRetryScreen()
    func showLoadingDialog()
    func showInvalidVersionScreen()
    func dismissLoadingDialog()
    func onError(title: String)
    func onAppLogout()
    func showEmptyItem()
    func loadInitialData()
    //func orderAddedToCart()
    //func hideContent()
}

open class DBLoadingPresenter<T: DBLoadingView>: DBBasePresenter{
    
    public weak var view: T?
    public var disposeBag = DisposeBag()
    private var coreDataAuthManager: DBCoreDataAuthProtocol?
    
    public init(view: T?, coreDataAuthManager: DBCoreDataAuthProtocol?) {
        self.view = view
        self.coreDataAuthManager = coreDataAuthManager
    }
    
    open func loggedInSuccess() {
        DBAppLocalNotification.sendLocalNotification(notificationType: .refreshScreen)
    }
    
    
    open func onAppDefaultError(error: Error) {
        view?.dismissLoadingDialog()
        let apiError = error as! DBApiError
        if (apiError.errorType == DBApiErrorEnum.internalServerError) {
            view?.onError(title: NSLocalizedString("NETWORK_CODE_INTERNAL", comment: ""))
            return
        }
        if (apiError.errorType == DBApiErrorEnum.unAuthorized) {
            if (self.coreDataAuthManager?.getToken() == nil) {
                view?.onError(title: NSLocalizedString("NETWORK_CODE_INTERNAL", comment: ""))
                return
            }
            self.logoutApp()
            return
        }
        
        if (apiError.errorType == DBApiErrorEnum.someAppError) {
            view?.onError(title: NSLocalizedString("NETWORK_CODE_\(apiError.code!)", comment: ""))
            return
        }
        if (apiError.errorType == DBApiErrorEnum.unAuthorized) {
            self.logoutApp()
            return
        }
    }
    
    open func onAppRetryError(error: Error) {
        self.view?.dismissLoadingDialog()
        view?.showContent()
        let apiError = error as! DBApiError
        if (apiError.errorType == DBApiErrorEnum.internalServerError || apiError.errorType == DBApiErrorEnum.someAppError) {
            view?.showRetryScreen()
            return
        }
        if (apiError.errorType == DBApiErrorEnum.invalidVersion) {
            view?.showInvalidVersionScreen()
            return
        }
        if (apiError.errorType == DBApiErrorEnum.unAuthorized) {
            if (self.coreDataAuthManager?.getToken() == nil) {
                self.view?.showRetryScreen()
                return
            }
            self.logoutApp()
            return
        }
    }
    
    open func logoutApp(){
        coreDataAuthManager?.logoutUser()
    }
    
    open func start() {
        print(getClassName()+"_refreshScreen")
        DBAppLocalNotification.addLocalSubscriber(key: getClassName()+"_refreshScreen", localEventType: [.refreshScreen]) { (localEvent) in
            print("_refreshScreen - \(self.getClassName())")
            self.view?.loadInitialData()
        }
    }
    
    open func stop() {
        DBAppLocalNotification.removeLocalSubscriber(key: getClassName()+"_refreshScreen")
        DBAppLocalNotification.removeLocalSubscriber(key: getClassName())
    }
    
    public func getClassName() -> String {
        return String(describing: type(of: self))
    }
    
    deinit {
        stop()
        print("\(getClassName()) deinit")
    }
}

public protocol DBTabBarLoadingView: AnyObject {
    
}

open class DBTabBarLoadingPresenter<T: DBTabBarLoadingView>: DBBasePresenter{
    
    public weak var view: T?
    public var disposeBag = DisposeBag()
    
    public init(view: T) {
        self.view = view
    }
    public func getClassName() -> String {
        return String(describing: type(of: self))
    }
    
    open func start() {
        
    }
    
    open func stop() {
        DBAppLocalNotification.removeLocalSubscriber(key: getClassName())
    }
    
    deinit {
        stop()
    }
}
