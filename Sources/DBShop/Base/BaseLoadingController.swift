//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 28.05.2021.
//

import Foundation
import UIKit
import DBTableCollectionView
import Toast



open class DBBaseLoadingViewController: UIViewController, DBLoadingView {
    
    //var formatter = YUPhoneFormatter()
    //var formatter = YUPhoneFormatter()
    @IBOutlet public weak var scrollView: UIScrollView!
    public var strPhoneNumber = "+7 (___) ___-__-__";
    
    var activityView : UIView?
    var isLoading : Bool = false
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = DBVariables.tableBackgroundColor
        //addKeyboardNotification()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        //self.hideView()
    }
    
    open func loadInitialData() {
        
    }
    
    public func getBundle() -> Bundle{
        return Bundle.module
    }
    
    public func initDBTableView(style: DBTableViewStyle = .plain, topAnchor: NSLayoutYAxisAnchor? = nil) -> DBTableView {
        let tableView = DBTableView(frame: CGRect.zero, style: style)
        //        var frame = CGRect.zero
        //        frame.size.height = .leastNormalMagnitude
        //        tableView.tableHeaderView = UIView(frame: frame)
        //tableView.removeTopSpace()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        //tableView.layoutAttachAll()
        if (topAnchor == nil) {
            if #available(iOS 11.0, *) {
                tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
            }else{
                tableView.topAnchor.constraint(equalTo: self.topLayoutGuide.topAnchor).isActive = true
            }
        }else{
            tableView.topAnchor.constraint(equalTo: topAnchor!).isActive = true
        }

        tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        //tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        if #available(iOS 11.0, *) {
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        }else{
            tableView.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.bottomAnchor).isActive = true
        }
         
        
        
        tableView.backgroundColor = DBVariables.tableBackgroundColor
        self.initRefreshControl(tv: tableView)
        return tableView
    }
    
    public func initCollectionView() -> DBCollectionView {
        let collectionView = DBCollectionView()
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(collectionView)
        collectionView.layoutAttachAll()
        collectionView.backgroundColor = DBVariables.tableBackgroundColor
        self.initRefreshControl(tv: collectionView)
        return collectionView
    }
    
    public func initRefreshControl(tv: UIView) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        tv.addSubview(refreshControl) // not required when using UITableViewController
    }
    @objc open func refresh(sender:UIRefreshControl) {
        sender.endRefreshing()
        self.loadInitialData()
    }

    
    @objc public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
            self.scrollView?.scrollRectToVisible(nextField.frame, animated: true)
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    open func showEmptyItem() {
        
    }
    
    public func initDBPagingTableView(style: DBTableViewStyle = .plain) -> DBPagingTableView {
        let tableView = DBPagingTableView(frame: CGRect.zero, style: style)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = DBVariables.tableBackgroundColor
        view.addSubview(tableView)
        tableView.layoutAttachAll()
        self.initRefreshControl(tv: tableView)
        return tableView
    }
    

    
    public func addKeyboardNotification(){
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(DBBaseLoadingViewController.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(DBBaseLoadingViewController.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap = UITapGestureRecognizer(target: self, action: #selector(viewTap))
        tap.cancelsTouchesInView = true
        self.view.addGestureRecognizer(tap)
    }
    
    @objc public func viewTap(tap : UITapGestureRecognizer){
        self.view.endEditing(false)
    }
    
    @objc public func keyboardWillShow(notification: NSNotification) {
        
        if scrollView != nil {
            let size = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                if let bottom = window?.safeAreaInsets.bottom {
                    self.scrollView.contentInset.bottom = (size?.height)! - bottom
                } else {
                    self.scrollView.contentInset.bottom = (size?.height)!
                }
            } else {
                self.scrollView.contentInset.bottom = (size?.height)!
            }
        }
    }
    
    @objc public func keyboardWillHide(notification: NSNotification) {
        if scrollView != nil {
            self.scrollView.contentInset.bottom = 0
        }
    }
    
    
    
    private func hideView() {
        clerViewWith(tag: 101)
        let whiteView = UIView()
        self.view.addSubview(whiteView)
        whiteView.backgroundColor = .white
        whiteView.translatesAutoresizingMaskIntoConstraints = false
        whiteView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        whiteView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        whiteView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        whiteView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        
        whiteView.alpha  = 0
        whiteView.tag = 101
        whiteView.alpha = 1
    }
    
    public func addCheckForEmpty(textField: UITextField, button: UIButton) {
        //textField.addTarget(self, action: #selector(editingChanged), for: .editingChanged)
        textField.actionHandler(controlEvents: .editingChanged) {
            if textField.text?.count == 1 {
                if textField.text?.first == " " {
                    textField.text = ""
                    return
                }
            }
            guard
                let text = textField.text, !text.isEmpty
            else {
                button.isEnabled = false
                return
            }
            button.isEnabled = true
        }
    }
    
    
    private func clerViewWith(tag: Int) {
        self.view.subviews.forEach { (view) in
            if (view.tag == tag) {
                view.removeFromSuperview()
            }
        }
    }
    
    private func showView() { // убирает белый view
        self.view.subviews.forEach { (view) in
            if (view.tag == 101) {
                UIView.animate(withDuration: 0.2, animations: {
                    view.alpha = 0
                }, completion: { (boolean) in
                    view.removeFromSuperview()
                })
            }
        }
    }
    
    public func onError(title: String) {
        self.view.makeToast(title, duration: 1.0, position: .bottom)
    }
    
    private func showErrorView(){
        clerViewWith(tag: 10)
        let errorView = ErrorRetryView.fromNib(nibName: "ErrorRetryView", bundle: Bundle.module)
        errorView.messageLabel.text = NSLocalizedString("SERVER_ERROR_STRING", bundle: Bundle.module, comment: "")
        errorView.retryButton.setTitle(NSLocalizedString("SERVER_ERROR_BUTTON_TITLE", bundle: Bundle.module, comment: ""), for: .normal)
        errorView.frame = self.view.bounds
        errorView.alpha = 0
        errorView.tag = 10
        errorView.retryButton.addTarget(self, action: #selector(onRetryButtonAction), for: .touchUpInside)
        self.view.addSubview(errorView)
        UIView.animate(withDuration: 0.2) {
            errorView.alpha = 1
        }
    }
    
    
    private func needVersionUpdateServerError(){
        hideErrorView()
        let errorView = ErrorRetryView.fromNib(nibName: "ErrorRetryView", bundle: Bundle.module)
        errorView.messageLabel.text = NSLocalizedString("OLD_APP_VERSION_TITLE", bundle: Bundle.module, comment: "")
        errorView.retryButton.setTitle(NSLocalizedString("REFRESH_BUTTON_TITLE", bundle: Bundle.module, comment: ""), for: .normal)
        errorView.frame = self.view.bounds
        errorView.alpha = 0
        errorView.tag = 10
        errorView.retryButton.addTarget(self, action: #selector(updateVersionButtonAction), for: .touchUpInside)
        self.view.addSubview(errorView)
        UIView.animate(withDuration: 0.2) {
            errorView.alpha = 1
        }
    }
    
    private func hideErrorView() {
        clerViewWith(tag: 10)
    }
    
    
    @objc public func onRetryButtonAction(){
        self.loadInitialData()
        hideErrorView()
    }
    
    @objc private func updateVersionButtonAction(){
        self.loadInitialData()
        hideErrorView()
        var appString = String()
        appString = NSLocalizedString("ITUNES_APP_LINK", comment: "")
        
        guard let url = URL(string: appString) else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    open func onAppLogout() {
        //Router.logoutApp(vc: self)
        fatalError("need to override")
    }
    
    public func showToastErrorWithMessages(str: String) {
        //self.view.makeToast(str, duration: 1.0, position: .center)
    }
    
    //MARK:- LoadingView impl
    public func showLoading() {
        hideView()
        hideErrorView()
        showLoader()
    }
    
    public func showContent() {
        hideLoader()
        hideErrorView()
        showView()
    }
    
    public func showRetryScreen() {
        hideLoader()
        hideView()
        showErrorView()
    }
    
    public func showInvalidVersionScreen() {
        hideLoader()
        hideView()
        needVersionUpdateServerError()
    }
    
    public func showLoadingDialog() {
        showLoader()
    }
    
    public func dismissLoadingDialog() {
        hideLoader()
    }
    
    //MARK:- IMAGE PICKER
    //UINavigationControllerDelegate, UIImagePickerControllerDelegate, CropViewControllerDelegate
    public func showImagePicker(vc: UIViewController, delegate: UINavigationControllerDelegate & UIImagePickerControllerDelegate){
        DBAlert.cameraOrLibrary(vc: vc) {
            self.openImagePicker(vc: vc, .camera, delegate: delegate)
        } libraryAction: {
            self.openImagePicker(vc: vc, .photoLibrary, delegate: delegate)
        }
    }
    
    private func openImagePicker(vc: UIViewController, _ type : UIImagePickerController.SourceType, delegate: UINavigationControllerDelegate & UIImagePickerControllerDelegate) {
        let imag = UIImagePickerController()
        imag.modalPresentationStyle = .overCurrentContext
        imag.delegate = delegate
        imag.sourceType = type
        imag.allowsEditing = false
        vc.present(imag, animated: true, completion: nil)
    }
    
    
    open func imagePickerCompleted(image: UIImage) {
        
    }
    
    public func showImageCrop(vc: UIViewController, image: UIImage, delegate: CropViewControllerDelegate){
        let controller = CropViewController()
        controller.modalPresentationStyle = .overCurrentContext
        controller.cropAspectRatio = 1.0
        controller.keepAspectRatio = true
        controller.delegate = delegate
        controller.image = image
        vc.present(controller, animated: true, completion: nil)
    }
    
    open func imageCropCompleted(image: UIImage) {
        
    }
    
    @objc public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        picker.dismiss(animated: true, completion: nil)
        self.imagePickerCompleted(image: image)
    }
    
    public func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage, transform: CGAffineTransform, cropRect: CGRect) {
        
    }
    
    public func cropViewController(_ controller: CropViewController, didFinishCroppingImage image: UIImage) {
        controller.dismiss(animated: true, completion: nil)
        self.imageCropCompleted(image: image)
    }
    
    public func cropViewControllerDidCancel(_ controller: CropViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
   
    
    func showLoader(){
        let window = self.view
        if activityView == nil {
            activityView = UIView(frame: window!.frame)
            activityView!.layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
            let shadowView = UIView(frame: activityView!.frame)
            shadowView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
            activityView?.addSubview(shadowView)
            
            let indicatorView = UIView(frame: CGRect(x: activityView!.center.x - 35, y: activityView!.center.y - 35, width: 70, height: 70))
            indicatorView.backgroundColor = UIColor.black
            indicatorView.layer.cornerRadius = 20
            indicatorView.layer.masksToBounds = true
            
            let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
            activityIndicatorView.center = CGPoint(x: indicatorView.bounds.size.width/2, y: indicatorView.bounds.size.height/2)
            
            activityIndicatorView.color = UIColor.white
            
            indicatorView.addSubview(activityIndicatorView)
            indicatorView.bringSubviewToFront(activityIndicatorView)
            
            activityIndicatorView.startAnimating()
            activityView!.addSubview(indicatorView)
        }
        if !isLoading{
        window?.addSubview(activityView!)
            isLoading = true
        }
    }
    
    func hideLoader(){
        if activityView != nil && isLoading == true{
            isLoading = false
            activityView!.removeFromSuperview()
        }
    }
    
    deinit {
        print("\(String(describing: type(of: self))) deinit")
    }
    
    
    
}

class ErrorRetryView : UIView {
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet var retryButton: DBButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        retryButton.primary()
    }
    
    deinit {
        print("ErrorRetryView deinit")
    }
    
}



