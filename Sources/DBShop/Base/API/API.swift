//
//  Api.swift
//  youradvisor
//
//  Created by Dmitry Bogomazov on 20.03.2020.
//  Copyright © 2020 iCApps. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

open class DBResponseWrapped<T: Codable>: Codable {
    public var response: T
    init(initialValue value: T) { self.response = value }
}

public enum DBApiErrorEnum {
    case unAuthorized
    case internalServerError
    case someAppError
    case invalidVersion
}

internal class DBJSONFailure: Error, Codable {
    var status: String
    var code: Int
}

public class DBApiError: Error {
    
    public var errorType: DBApiErrorEnum!
    public var code: Int!
    
    init(errorType: DBApiErrorEnum) {
        self.errorType = errorType
        code = 500
    }
}



open class DBAPIManager {
    
    open class var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy { .convertFromSnakeCase }
    
    static func getDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .custom { decoder in
            let container = try decoder.singleValueContainer()
            let dateString = try container.decode(String.self)
            
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone.init(abbreviation: "GMT")
            formatter.locale = Locale.current
            
            formatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss"
            if let date = formatter.date(from: dateString) {
                return date
            }
            formatter.dateFormat = "yyyy-MM-dd' 'HH:mm"
            if let date = formatter.date(from: dateString) {
                return date
            }
            formatter.dateFormat = "yyyy-MM-dd' 'HH:mm:ss.SSS"
            if let date = formatter.date(from: dateString) {
                return date
            }
            throw DecodingError.dataCorruptedError(in: container,
                                                   debugDescription: "Cannot decode date string \(dateString)")
        }
        
        decoder.keyDecodingStrategy = keyDecodingStrategy
        return decoder
    }
    
    public static func request<T: Codable> (_ urlConvertible: URLRequestConvertible) -> Single<T> {
        return Single<T>.create { observer in
            let request = AF.request(urlConvertible).validate()
                .responseDecodable(decoder: getDecoder(), completionHandler: { (response: DataResponse<T, AFError>) in
                    print(response.error.debugDescription)
                    if (response.data != nil) {
                        print(String(data: response.data!, encoding: String.Encoding.utf8))                    }
                    
                    switch response.result {
                    case .success(let value):
                        observer(.success(value))
                    case .failure( _):
                        observer(.failure(checkResponse(response: response.data)))
                    }
                })
            // AF.request(urlConvertible).responseDec
            request.resume()
            return Disposables.create{
                request.cancel()
            }
        }
    }
    
    static func checkSuccessResponse(response: Data?) throws -> Dictionary<String, AnyObject>{
        let anyObj = try JSONSerialization.jsonObject(with: response!) as! Dictionary<String,AnyObject>
        return anyObj["response"] as! Dictionary<String, AnyObject>
    }
    
    public static func request(_ urlConvertible: URLRequestConvertible) -> Completable {
        return Completable.create { observer in
            let request = AF.request(urlConvertible).validate()
                .responseJSON { (response) in
                    switch response.result {
                    case .success( _):
                        observer(.completed)
                    case .failure( _):
                        observer(.error(checkResponse(response: response.data)))
                    }
            }
            request.resume()
            return Disposables.create{
                //request.cancel()
            }
        }
    }
    
    
    static func checkResponse(response: Data?) -> DBApiError{
        let apiError = DBApiError(errorType: .internalServerError)
        if (response == nil) {
            return apiError
        }
        do {
            let result = try JSONDecoder().decode(DBJSONFailure.self, from: response!)
            apiError.code = result.code
            if (result.code == 22) {
                apiError.errorType = DBApiErrorEnum.unAuthorized
            }else if (result.code == 26) {
                apiError.errorType = DBApiErrorEnum.invalidVersion
            } else{
                apiError.errorType = DBApiErrorEnum.someAppError
            }
        } catch {
            return apiError
        }
        return apiError
    }
}
