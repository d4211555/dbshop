//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 06.09.2021.
//

import Foundation
import UIKit
import Agrume

public class DBRouter {
    
    public class func openFullScreenImage(vc: UIViewController, image: UIImage) {
        let button = UIBarButtonItem(barButtonSystemItem: .stop, target: nil, action: nil)
        button.tintColor = .blue
        let agrume = Agrume(image: image, background: .colored(.white), dismissal: .withButton(button))
        agrume.show(from: vc)
    }
    
}
