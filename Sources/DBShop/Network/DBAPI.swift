//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 04.08.2021.
//

import Foundation
import Alamofire
struct DBNetworkConstants {
    
    static let dadataUrlAddress = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address"
    static let cdkUrlAddress = "http://" + DBConfig.stringValue(forKey: "CDK url")
    
    //The header fields
    enum HttpHeaderField: String {
        case authentication = "Authorization"
        case contentType = "Content-Type"
        case acceptType = "Accept"
        case warehouse = "WAREHOUSE"
    }
    
    //The content type (JSON)
    enum ContentType: String {
        case json = "application/json"
    }
}

class DBDaDataAPIRequest: URLRequestConvertible {
    var path: String
    var method: HTTPMethod
    var parameters: Parameters?
    
    init(path: String, method: HTTPMethod, parameters: Parameters?) {
        self.path = path
        self.method = method
        self.parameters = parameters
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try DBNetworkConstants.dadataUrlAddress.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.setValue(DBNetworkConstants.ContentType.json.rawValue, forHTTPHeaderField: DBNetworkConstants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(DBNetworkConstants.ContentType.json.rawValue, forHTTPHeaderField: DBNetworkConstants.HttpHeaderField.contentType.rawValue)
        
    
        let token = Bundle.main.object(forInfoDictionaryKey: "DaDataApiKey") as? String
        if (token != nil) {
            urlRequest.setValue("Token " + token!, forHTTPHeaderField: DBNetworkConstants.HttpHeaderField.authentication.rawValue)
        }
        
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding(arrayEncoding: .noBrackets)
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
}

//public enum ParameterEncoding {
//    case URL(brackets: Bool)
//}


class DBCDKAPIManager: DBAPIManager {
    override public class var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy { .useDefaultKeys }
}

class DBCDKAPIRequest: URLRequestConvertible {
    var path: String
    var method: HTTPMethod
    var parameters: Parameters?
    
    init(path: String, method: HTTPMethod, parameters: Parameters?) {
        self.path = path
        self.method = method
        self.parameters = parameters
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try DBNetworkConstants.cdkUrlAddress.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        urlRequest.httpMethod = method.rawValue
        
        urlRequest.setValue(DBNetworkConstants.ContentType.json.rawValue, forHTTPHeaderField: DBNetworkConstants.HttpHeaderField.acceptType.rawValue)
        urlRequest.setValue(DBNetworkConstants.ContentType.json.rawValue, forHTTPHeaderField: DBNetworkConstants.HttpHeaderField.contentType.rawValue)
        

        urlRequest.setValue("782", forHTTPHeaderField: DBNetworkConstants.HttpHeaderField.warehouse.rawValue)
        
        let encoding: ParameterEncoding = {
            switch method {
            case .get:
                return URLEncoding(arrayEncoding: .noBrackets)
            default:
                return JSONEncoding.default
            }
        }()
        
        return try encoding.encode(urlRequest, with: parameters)
    }
}

enum DBConfig {
  static func stringValue(forKey key: String) -> String {
      print(key)
      var s = Bundle.main.object(forInfoDictionaryKey: key)
      print(s)
    guard let value = Bundle.main.object(forInfoDictionaryKey: key) as? String
    else {
      fatalError("Invalid value or undefined key")
    }
    return value
  }
}
