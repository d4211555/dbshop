//
//  File.swift
//  
//
//  Created by Dmitrii on 05.09.2023.
//

import Foundation
import RxSwift
import Alamofire

class DBCDKService {
    
    static func getTariffs(address: String, mass: Int, declaredValue: Double?) -> Single<DBResponseWrapped<DBCDKMainResponse>> {
        var params: Parameters = ["address": address, "mass": mass]
        print(params)
        if declaredValue != nil {
            params.updateValue(declaredValue as Any, forKey: "declaredValue")
        }
        let request = DBCDKAPIRequest(path: "get_tariffs", method: .get, parameters:params)
        return DBAPIManager.request(request)
    }

//    static func checkPvz() -> Single<DBResponseWrapped<StockListResponse>> {
//        let request = DBCDKAPIRequest(path: "v1/get_short_stocks", method: .get, parameters: [:])
//        return APIManager.request(request)
//    }
//
//    static func createOrder(id: Int) -> Single<DBResponseWrapped<StockResponse>> {
//        let request = DBCDKAPIRequest(path: "v1/get_stock_by_id", method: .get, parameters: ["stockId":id])
//        return APIManager.request(request)
//    }
    
    //
    
    //get_short_stocks
}
