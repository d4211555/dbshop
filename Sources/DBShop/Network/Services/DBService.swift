//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 04.08.2021.
//

import Foundation
import RxSwift
import Alamofire

public class DBService {
    static func getAddresses(query: String, type: DBAddressType) -> Single<DaDataWrappedResponse> {
        let request = DBDaDataAPIRequest(path: "", method: .post, parameters: getAddressParameters(query: query, type: type))
        return DBAPIManager.request(request)
    }
    
    private static func getAddressParameters(query: String, type: DBAddressType) -> Parameters{
        var p = Parameters()
        p.updateValue(query, forKey: "query")
        if (type == .cities) {
            p.updateValue(["value": "city"] as Any, forKey: "from_bound")
            p.updateValue(["value": "settlement"] as Any, forKey: "to_bound")
        }else{
            p.updateValue(["value": "house"] as Any, forKey: "from_bound")
            p.updateValue(["value": "house"] as Any, forKey: "to_bound")
        }
        return p
    }
    
}

class DBDataSource {
    static func getAddresses(query: String, type: DBAddressType) -> Single<[DaDataResponse]>{
        let s =  DBService.getAddresses(query: query, type: type).map { d in
            return d.suggestions.filter{($0.data?.city != nil || $0.data?.settlement != nil) && $0.value != nil }
        }
        return s
    }
}

class DaDataWrappedResponse: Codable {
    var suggestions: [DaDataResponse]
}

class DaDataResponse: Codable {
    var data: DaDataDataResponse?
    var value: String?
    var unrestrictedValue: String?
}

class DaDataDataResponse: Codable {
    var city: String?
    var settlement: String?
    var geoLat: String?
    var geoLon: String?
    var fiasId: String?
}
