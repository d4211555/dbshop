//
//  File.swift
//  
//
//  Created by Dmitrii on 05.09.2023.
//

import Foundation
import RxSwift

public class DBCDKDataSource {
    public static func getTariffs(address: String, mass: Int, declaredValue: Double?) -> Single<[DBCDKGetTariffResponse]> {
        return DBCDKService.getTariffs(address: address, mass: mass, declaredValue: declaredValue).map { response in
            return response.response.cdk
        }
    }
//    static func getShortStocks() -> Single<[Stock]> {
//        return StockService.getShortStocks().map { response in
//            return response.response.stocks
//        }
//    }
//
//    static func getStock(id: Int) -> Single<Stock> {
//        return StockService.getStock(id: id).map { response in
//            return response.response.stock
//        }
//    }
}
