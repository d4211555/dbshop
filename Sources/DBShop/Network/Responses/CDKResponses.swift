//
//  File.swift
//  
//
//  Created by Dmitrii on 05.09.2023.
//

import Foundation


/* "name": "Посылка склад-дверь",
 "description": "",
 "totalPrice": 53000,
 "mailType": "PARCEL_WAREHOUSE_TO_DOOR",
 "deliveryTime": {
     "maxDays": 12,
     "minDays": 8
 }
*/
class DBCDKMainResponse: Codable  {
    var cdk: [DBCDKGetTariffResponse]
}

public class DBCDKGetTariffResponse: Codable {
    public var name: String
    public var description: String
    public var totalPrice: Int
    public var mailType: DBCDKTariffs
    public var deliveryTime: DBCDKGetTariffDeliveryTime?
    
}

public class DBCDKGetTariffDeliveryTime: Codable {
    public var maxDays: Int
    public var minDay: Int
}

public enum DBCDKTariffs: String, Codable {
    
    case parcelWarehouseToDoor = "PARCEL_WAREHOUSE_TO_DOOR"
    case parcelWarehouseToWarehouse = "PARCEL_WAREHOUSE_TO_WAREHOUSE"
    case parcelWarehouseToPostamat = "PARCEL_WAREHOUSE_TO_POSTAMAT"
    case expressWarehouseToDoor = "EXPRESS_WAREHOUSE_TO_DOOR"
    case expressWarehouseToWarehouse = "EXPRESS_WAREHOUSE_TO_WAREHOUSE"
    case expressWarehouseToPostamat = "EXPRESS_WAREHOUSE_TO_POSTAMAT"
    
    func getDescription() -> String {
        switch self {
        case .parcelWarehouseToDoor:
            return "Посылка склад-дверь"
        case .parcelWarehouseToWarehouse:
            return "Посылка склад-склад"
        case .parcelWarehouseToPostamat:
            return "Посылка склад-постамат"
        case .expressWarehouseToDoor:
            return "Экспесс склад-дверь"
        case .expressWarehouseToWarehouse:
            return "Экспресс склад-склад"
        case .expressWarehouseToPostamat:
            return "Экспресс склад-постамат"
        }
    }
}
