//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 04.08.2021.
//

import Foundation
import RxSwift
import UIKit
import DBTableCollectionView

public protocol DBAddressViewControllerDelegate: DBProtocol {
    func addressSelected(item: DBDaDataAddress)
}

public class DBAddressViewController: DBBaseLoadingViewController, DBAddressControllerPresenterDelegate, UISearchBarDelegate, AddressTableViewCellDelegate, DBTableViewAdapterDelegate {
    
    var tableView: DBTableView!
    var searchBar = UISearchBar()
    var presenter: DBAddressControllerPresenter<DBAddressViewController>? = nil
    var section = DBTableViewSection()
    var addressType: DBAddressType!
    weak var delegate: DBAddressViewControllerDelegate!
    
    public required init(title: String, addressType: DBAddressType, delegate: DBAddressViewControllerDelegate) {
        super.init(nibName: nil, bundle: nil)
        self.title = title
        self.addressType = addressType
        self.delegate = delegate
    }
    
    internal required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    public override func show(_ vc: UIViewController, sender: Any?) {
        if #available(iOS 13.0, *) {
            self.modalPresentationStyle = .automatic
        } else {
            self.modalPresentationStyle = .overCurrentContext
        }
        vc.present(self, animated: true, completion: nil)
    }
    
    public override func viewDidLoad() {
        self.presenter = DBAddressControllerPresenter(view: self, coreDataAuthManager: nil)
        self.view.backgroundColor = DBColors.backgroundGrayColor
        let navbar = UINavigationBar()
        navbar.delegate = self as? UINavigationBarDelegate
        let navItem = UINavigationItem()
        navItem.title = self.title
        
        let closeButton = DBViewHelper.getCloseButton()
        closeButton.addAction { [unowned self] in
            self.dismiss(animated: true, completion: nil)
        }
        navItem.setRightBarButton(UIBarButtonItem(customView: closeButton), animated: false)
        
        navbar.items = [navItem]
        view.addSubview(navbar)
        navbar.translatesAutoresizingMaskIntoConstraints = false
        if #available(iOS 11.0, *) {
            navbar.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            navbar.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: 20).isActive = true
        }
        navbar.layoutAttachLeading()
        navbar.layoutAttachTrailing()
        
        self.view.addSubview(searchBar)
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        searchBar.layoutAttachLeading()
        searchBar.layoutAttachTrailing()
        searchBar.topAnchor.constraint(equalTo: navbar.bottomAnchor, constant: 0.5).isActive = true
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        searchBar.barTintColor = UIColor.white
        self.searchBar.searchBarStyle = .minimal
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
        self.tableView = initDBTableView(topAnchor: searchBar.bottomAnchor)
        self.tableView.tableViewDelegate = self
        self.tableView.registerCell(cell: AddressTableViewCell.getConfiguration(delegate: self))
        self.tableView.add(section: section)
    }
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func dataLoaded(list: [DaDataResponse]) {
        self.tableView.replaceAll(items: list.map{return DBDaDataAddress(fullValue: $0.unrestrictedValue, value: $0.value, geoLon: $0.data?.geoLon, geoLat: $0.data?.geoLat)}, in: section)
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter?.getAddresses(query: searchBar.text!, type: addressType)
    }
    
    func cellDidSelect(item: DBDaDataAddress) {
        self.delegate.addressSelected(item: item)
        self.dismiss(animated: true, completion: nil)
    }
}


public class DBDaDataAddress: DBModel, Codable {
    public var fullValue: String?
    public var value: String?
    public var geoLon: String?
    public var geoLat: String?
    init(fullValue: String?, value: String?, geoLon: String?, geoLat: String?) {
        self.fullValue = fullValue
        self.value = value
        self.geoLon = geoLon
        self.geoLat = geoLat
    }
}

public enum DBAddressType {
    case cities
    case fullAddress
}

protocol AddressTableViewCellDelegate: DBProtocol {
    func cellDidSelect(item: DBDaDataAddress)
}

class AddressTableViewCell: DBTableViewCell<DBDaDataAddress, AddressTableViewCellDelegate> {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: "AddressTableViewCell")
        self.textLabel?.numberOfLines = 0
        self.selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func getConfiguration(delegate: AddressTableViewCellDelegate?) -> DBTableViewCellConfig{
        return super.getConfiguration(delegate: delegate, nibName: nil, registerType: .registerClass)
    }
    
    override func configureCell(item: DBDaDataAddress) {
        //self.detailTextLabel?.text = item.city
        self.textLabel?.text = item.value
    }
    
    override func cellDidSelect(item: DBDaDataAddress) {
        getDelegate()?.cellDidSelect(item: item)
    }
}
