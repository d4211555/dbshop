//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 04.08.2021.
//

import Foundation
import RxSwift


protocol DBAddressControllerPresenterDelegate: DBLoadingView {
    func dataLoaded(list: [DaDataResponse])
}

class DBAddressControllerPresenter<T: DBAddressControllerPresenterDelegate>: DBLoadingPresenter<T>{
    
    func getAddresses(query: String, type: DBAddressType) {
        //self.view?.show
        DBDataSource.getAddresses(query: query, type: type)
            .observe(on: MainScheduler.instance)
            .subscribe(onSuccess: { (l) in
                self.view?.dataLoaded(list: l)
                //self.view?.showContent()
            }, onFailure: onAppDefaultError)
            .disposed(by: disposeBag)
    }
    
    
}
