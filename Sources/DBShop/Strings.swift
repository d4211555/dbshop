//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 24.08.2021.
//

import Foundation

open class DBStrings {
    public static var pleaseRateProduct = NSLocalizedString("PLEASE_RATE_PRODUCT", bundle: .module, comment: "")
    public static var addNewReview = NSLocalizedString("ADD_NEW_REVIEW", bundle: .module, comment: "")
    public static var addReview = NSLocalizedString("ADD_REVIEW", bundle: .module, comment: "")
    public static var commentTitle = NSLocalizedString("COMMENT_TITLE", bundle: .module, comment: "")
    public static var askManager = NSLocalizedString("ASK_MANAGER_ABOUT_PRODUCT", bundle: .module, comment: "")
    public static var payOrder = NSLocalizedString("PAY_ORDER", bundle: .module, comment: "")
    //MARK:- Product Attributes
    
    public static var productBrand = NSLocalizedString("PRODUCT_BRAND", bundle: .module, comment: "")
    public static var colorBrand = NSLocalizedString("PRODUCT_COLOR", bundle: .module, comment: "")
    public static var productType = NSLocalizedString("PRODUCT_TYPE", bundle: .module, comment: "")
    public static var removeAccount = NSLocalizedString("REMOVE_ACCOUNT", bundle: .module, comment: "")
    
    public static var inputDeliveryDate = NSLocalizedString("INPUT_DELIVERY_DATE", bundle: .module, comment: "")
    public static var inputDeliveryAddress = NSLocalizedString("INPUT_DELIVERY_ADDRESS", bundle: .module, comment: "")
    public static var inputDeliveryTime = NSLocalizedString("INPUT_DELIVERY_TIME", bundle: .module, comment: "")
    public static var inputDeliveryMethod = NSLocalizedString("INPUT_DELIVERY_METHOD", bundle: .module, comment: "")
    
    public static var removeProductFromCart = NSLocalizedString("REMOVE_PRODUCT_FROM_CART", bundle: .module, comment: "")
    
    
}
