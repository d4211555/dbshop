//
//  File.swift
//  
//
//  Created by Dmitry Bogomazov on 22.08.2021.
//

import Foundation
import UIKit
import DBTableCollectionView
import SDWebImage

public protocol DBReviewTableViewCellDelegate: DBProtocol {
    
}

public class DBReviewItem: DBModel {
    var id: Int!
    var comment: String?
    var value: Int!
    var date: String!
    var userFirstName: String!
    var userLastName: String!
    var userAvatar: URL!
    
    public init(id: Int, comment: String?, value: Int, date: String, userFirstName: String, userLastName: String, userAvatar: URL) {
        self.id = id
        self.comment = comment
        self.value = value
        self.date = date
        self.userFirstName = userFirstName
        self.userLastName = userLastName
        self.userAvatar = userAvatar
    }
    
}

public class DBReviewTableViewCell: DBTableViewCell<DBReviewItem, DBReviewTableViewCellDelegate> {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var ratingView: FloatRatingView!
    @IBOutlet weak var commentLabelBottomConstraint: NSLayoutConstraint!
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        self.userImageView.circle()
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.userImageView.circle()
        self.selectionStyle = .none
    }
    
    public override func configureCell(item: DBReviewItem) {
        if validateValue(value: item.comment, forConstraint: self.commentLabelBottomConstraint, onNilValueConstant: 0, onValidValueConstant: 16) {
            self.commentLabel.text = item.comment
        }
        
        self.userName.text = item.userFirstName + " " + "\(String(describing: item.userLastName.first!))."
        self.dateLabel.text = item.date
        self.userImageView.sd_setImage(with: item.userAvatar,
                                    placeholderImage: DBImage.avatarPlaceholder(),
                                         options: .refreshCached)
        self.ratingView.rating = Double.init(item.value)
        
    }
    
    open class func getConfiguration(delegate: DBReviewTableViewCellDelegate?) -> DBTableViewCellConfig{
        return super.getConfiguration(delegate: delegate, nibName: "DBReviewTableViewCell", bundle: Bundle.module, registerType: .registerNib)
    }
    
}
