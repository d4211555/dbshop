//
//  File.swift
//  
//
//  Created by Dmitrii on 04.08.2023.
//

import Foundation
import DBTableCollectionView

open class DBCategory: DBModel, Codable {
    open var name: String
    
    open func getThumbImageUrl() -> String {
        fatalError("need to override")
    }
    
}
