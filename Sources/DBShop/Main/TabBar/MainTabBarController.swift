//
//  MainTabBarController.swift
//  NewCosmedexOrgApp
//
//  Created by Дима on 06.06.16.
//  Copyright © 2016 Дима. All rights reserved.
//

import UIKit

open class DBMainTabBarController: UITabBarController, UITabBarControllerDelegate, DBMainTabBarControllerPresenterDelegate {
    

    open var cartTabBarItem: UITabBarItem? = nil
    open var favouriteTabBarItem: UITabBarItem? = nil
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    public func embedToNavigation(vc: UIViewController, tabBarItem: UITabBarItem) -> UINavigationController {
        vc.navigationItem.title = tabBarItem.title
        let navVc = UINavigationController(rootViewController: vc)
        
        navVc.tabBarItem = tabBarItem
        return navVc
    }

    open func setCartItemBadge(count: Int) {
        if (count != 0) {
            cartTabBarItem?.badgeValue = "\(count)"
        }else{
            cartTabBarItem?.badgeValue = nil
        }
    }

    open func setFavouriteItemBadge(count: Int) {
        if (count != 0) {
            favouriteTabBarItem?.badgeValue = "\(count)"
        }else{
            favouriteTabBarItem?.badgeValue = nil
        }
    }
    
}
