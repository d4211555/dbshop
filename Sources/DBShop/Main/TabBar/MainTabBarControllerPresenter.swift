//
//  TabBarControllerPresenter.swift
//  CosmedexiOSClient
//
//  Created by Dmitry Bogomazov on 24.06.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import RxSwift

public protocol DBMainTabBarControllerPresenterDelegate: DBTabBarLoadingView {
    func setCartItemBadge(count: Int)
    func setFavouriteItemBadge(count: Int)
}

//public protocol DBNotificationsPresenter {
//    //associatedtype Cart
//    //associatedtype Event
//    func getEventCountDataSource<T>() -> Single<[T]>
//    func getCartCountDataSource<T>() -> [T]
//}
//
open class DBMainTabBarControllerPresenter<T: DBMainTabBarControllerPresenterDelegate, Favourite: DBFavouriteItemModel, CartModel: DBCartItemModel>: DBTabBarLoadingPresenter<T>{

    
    override open func start() {
        super.start()
        DBAppLocalNotification.addLocalSubscriber(key: getClassName()+"_DBMainTabBarControllerPresenter", localEventType: [.updateOrderBadge, .updateFavouritesCount]) { (localEvent) in
            if (localEvent.type == DBChangesNotificationType.updateOrderBadge) {
                self.view?.setCartItemBadge(count: DBRuntimeStorageSingleton.sharedInstance.getCartItemsCount())
            }
            if (localEvent.type == DBChangesNotificationType.updateFavouritesCount) {
                self.view?.setFavouriteItemBadge(count: DBRuntimeStorageSingleton.sharedInstance.getRuntimeFavouriteCount())
            }
        }
        self.loadFavouriteItems()
        self.loadCartItems()
    }
    
    open func getFavouriteCountDataSource() -> [Favourite]{
        fatalError("need to override to get favourite count")
    }
    
//    open func getEventCountDataSource() -> Single<[Event]>{
//        fatalError("need to override to get favourite count")
//    }
    
    open func getCartCountDataSource() -> [CartModel]{
        fatalError("need to override to get cart item count")
    }
//
    func loadFavouriteItems() {
        let favourites = getFavouriteCountDataSource()
        DBRuntimeStorageSingleton.sharedInstance.setRuntimeFavourites(favouriteItems: favourites)
        self.view?.setFavouriteItemBadge(count: DBRuntimeStorageSingleton.sharedInstance.getRuntimeFavouriteCount())
    }
    
    func loadCartItems() {
        let cartItems = getCartCountDataSource()
        cartItems.forEach { (cartItem) in
            DBRuntimeStorageSingleton.sharedInstance.addCartItem(productId: cartItem.productId, count: cartItem.count)
        }
        self.view?.setCartItemBadge(count: DBRuntimeStorageSingleton.sharedInstance.getCartItemsCount())
    }
    
    open override func stop() {
        super.stop()
        DBAppLocalNotification.removeLocalSubscriber(key: getClassName()+"_DBMainTabBarControllerPresenter")
    }
}
