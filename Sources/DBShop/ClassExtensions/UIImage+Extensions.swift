//
//  UIImage+Extensions.swift
//  CosmedexAdmin
//
//  Created by Dmitry Bogomazov on 16.03.2021.
//  Copyright © 2021 Дима. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {

    public convenience init?(barcode: String) {
        let data = barcode.data(using: .ascii)
        guard let filter = CIFilter(name: "CICode128BarcodeGenerator") else {
            return nil
        }
        filter.setValue(data, forKey: "inputMessage")
        
        guard var ciImage = filter.outputImage else {
            return nil
        }
            let imageSize = ciImage.extent.integral
            let outputSize = CGSize(width:640, height: 240)
            ciImage = ciImage.transformed(by:CGAffineTransform(scaleX: outputSize.width/imageSize.width, y: outputSize.height/imageSize.height))

        self.init(ciImage: ciImage)
    }
    
    func convertCIImageToUIImage(ciimage:CIImage)->UIImage{
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(ciimage, from: ciimage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    public func toBase64() -> String {
        let imageData = self.jpegData(compressionQuality: DBVariables.imageScale)
            let decodedString = imageData!.base64EncodedString()
            return decodedString
        }

}
