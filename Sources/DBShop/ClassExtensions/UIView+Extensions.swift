//
//  UIView+Extensions.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 03.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit
import DBTableCollectionView

extension UIView {
    
    public func removeAllSubviews() {
        self.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
    }
    
    public class func fromNib(nibName: String?, bundle: Bundle? = nil) -> Self {
        func fromNibHelper<T>(nibName: String?) -> T where T : UIView {
            let bundle = bundle == nil ? Bundle(for: T.self) : bundle
            let name = nibName ?? String(describing: T.self)
            return bundle!.loadNibNamed(name, owner: nil, options: nil)?.first as? T ?? T()
        }
        return fromNibHelper(nibName: nibName)
    }
    
    
    
    @discardableResult   // 1
    public func fromNib<T : UIView>() -> T? {   // 2
        guard let contentView = Bundle(for: type(of: self)).loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?.first as? T else {    // 3
            // xib not loaded, or its top view is of the wrong type
            return nil
        }
        self.addSubview(contentView)     // 4
        contentView.translatesAutoresizingMaskIntoConstraints = false   // 5
        contentView.layoutAttachAll()   // 6
        return contentView   // 7
    }
    
  
    
    public func addAction(action: @escaping () -> ()) {
        self.isUserInteractionEnabled = true
        let sleeve = ClosureSleeve(attachTo: self, closure: action)
        if (self is UITextField) {
            (self as! UITextField).addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: .allEditingEvents)
        }else{
            let gesture = UITapGestureRecognizer(target: sleeve, action: #selector(ClosureSleeve.invoke))
            self.addGestureRecognizer(gesture)
        }
    }
    
    //MARK: - SHADOW
    
    public func setDeselectShadowToView(){
        self.layer.shadowPath = UIBezierPath(roundedRect:self.bounds, cornerRadius:self.layer.cornerRadius).cgPath
        self.layer.shadowOffset = CGSize(width:0, height:0.5);
        self.layer.shadowRadius = 1;
        self.layer.shadowOpacity = 0.3;
    }
    
    public func addShadow(){
            self.layer.shadowOffset = CGSize(width:0, height:0.5);
            self.layer.shadowRadius = 1;
            self.layer.shadowOpacity = 0.3;
        }
    
    public func dropShadow() {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }
    
    //MARK: - BORDERS
    
    public func setTextFieldBorder() {
        self.layer.borderColor = UIColor.init(white: 0, alpha: 0.3).cgColor
        self.layer.borderWidth = 0.25
        self.layer.cornerRadius = 5
    }
    
    
    
    public func setBorderColor(width: CGFloat = DBVariables.itemBorderWidth, color: UIColor = DBVariables.itemBorderColor, cornerRadius: CGFloat = DBVariables.itemCornerRadius) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
        layer.cornerRadius = cornerRadius
    }
    
    public func roundCorners(corners: UIRectCorner, radius: CGFloat = DBVariables.itemCornerRadius) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    //MARK: - BULLSHIT
    private struct AssociatedKeys {
        static var layoutGuide = "layoutGuide"
    }
    
    public  var associatedLayoutGuide: UILayoutGuide? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.layoutGuide) as? UILayoutGuide
        }
        
        set {
            if let newValue = newValue {
                objc_setAssociatedObject(
                    self, &AssociatedKeys.layoutGuide,
                    newValue as UILayoutGuide?,
                    .OBJC_ASSOCIATION_RETAIN_NONATOMIC
                )
            }
        }
    }
}
