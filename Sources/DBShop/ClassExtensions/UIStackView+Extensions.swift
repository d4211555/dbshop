//
//  UIStackView+Extensions.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 03.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

extension UIStackView {
    public func removeAllSubview() {
        self.arrangedSubviews.forEach { (view) in
            view.removeFromSuperview()
        }
        self.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
    }
}
