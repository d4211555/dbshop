//
//  UITextField+Extensions.swift
//  youradvisor
//
//  Created by Dmitry Bogomazov on 25.03.2020.
//  Copyright © 2020 iCApps. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    private func actionHandler(action:(() -> Void)? = nil) {
        struct __ { static var action :(() -> Void)? }
        if action != nil { __.action = action }
        else { __.action?() }
    }
    @objc private func triggerActionHandler() {
        self.actionHandler()
    }
    public func actionHandler(controlEvents control :UIControl.Event, ForAction action:@escaping () -> Void) {
        self.actionHandler(action: action)
        self.addTarget(self, action: #selector(triggerActionHandler), for: control)
    }
    

}
