//
//  UIControl+Extensions.swift
//  CosmedexiOSClient
//
//  Created by Дмитрий on 03.04.2020.
//  Copyright © 2020 Dmitrii Bogomazov. All rights reserved.
//

import Foundation
import UIKit

extension UIControl {
    
    @objc public func addAction(for controlEvents: UIControl.Event = .editingChanged, action: @escaping () -> ()) {
        self.isUserInteractionEnabled = true
        let sleeve = ClosureSleeve(attachTo: self, closure: action)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
    }//textField.addTarget(self, action:Selector("textDidBeginEditing"), forControlEvents: UIControlEvents.EditingDidBegin)
}
