// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DBShop",
    defaultLocalization: "ru",
    platforms: [
        .iOS(.v11), .macOS(.v10_14)
        ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "DBShop",
            targets: ["DBShop"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(url: "https://d4211555@bitbucket.org/d4211555/dbtablecollectionview.git", .exact("1.0.3")),
        .package(url: "https://github.com/ReactiveX/RxSwift", .exact("6.2.0")),
        .package(name: "Firebase", url: "https://github.com/firebase/firebase-ios-sdk.git", "7.7.0"..<"8.0.0"),
        .package(url: "https://github.com/JanGorman/Agrume", "5.6.13"..<"5.6.13"),
        .package(url: "https://github.com/Alamofire/Alamofire", "5.4.3"..<"6.0.0"),
        .package(name: "Toast", url: "https://github.com/scalessec/Toast-Swift", "5.0.1"..<"6.0.0"),
        //.package(name: "SDWebImage", url: "https://github.com/SDWebImage/SDWebImage", "5.10.4"..<"6.0.0"),
        .package(name: "SDWebImage", url: "https://github.com/SDWebImage/SDWebImage", .exact("5.15.5")),
        //5.15.5
        .package(name: "Lottie", url: "https://github.com/airbnb/lottie-ios.git", "3.2.1"..<"3.2.3"),
        .package(url: "https://github.com/marmelroy/PhoneNumberKit", .upToNextMajor(from: "3.3.3"))

    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        
        .target(
            name: "DBShop",
            dependencies: ["RxSwift", "Alamofire", "Agrume", "Lottie", "PhoneNumberKit",
                           .product(name: "Toast", package: "Toast"),
                           .product(name: "DBTableCollectionView", package: "DBTableCollectionView"),
                           .product(name: "FirebaseMessaging", package: "Firebase"),
                           .product(name: "SDWebImage", package: "SDWebImage")
            ],
            resources: [.process("Resources")]),
        .testTarget(
            name: "DBShopTests",
            dependencies: ["DBShop"]),
    ]
    
    
    
)
